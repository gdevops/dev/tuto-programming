.. index::
   pair: datetime class; methods

.. _python_datetime_class_methods:

=====================================
Python **datetime class** methods
=====================================

.. toctree::
   :maxdepth: 3


   now/now
   today/today


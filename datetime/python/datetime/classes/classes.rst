.. index::
   pair: python ; datetime classes

.. _python_datetime_classes:

=====================================
Python **datetime classes**
=====================================

- https://docs.python.org/dev/library/datetime.html
- https://www.docstring.fr/blog/la-gestion-des-dates-avec-python/

.. toctree::
   :maxdepth: 3

   date/date
   datetime/datetime
   timedelta/timedelta
   tzinfo/tzinfo

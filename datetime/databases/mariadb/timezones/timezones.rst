
.. index::
   pair: MariaDB ; timezones

.. _prog_mariadb_timezones:

=====================================
MariaDB **timezones**
=====================================

.. seealso::

   - :ref:`mariadb_timezones`
   - :ref:`prog_postgresql_timezones`

.. index::
   pair: Python asynchronous ; sniffio
   ! sniffio

.. _sniffio:

================================================================================================
**sniffio** Sniff out which async library your code is running under
================================================================================================

.. seealso::

   - https://github.com/python-trio/sniffio
   - https://sniffio.readthedocs.io/en/latest/




Description
===========

You’re writing a library.

You’ve decided to be ambitious, and support multiple async I/O packages,
like Trio, and asyncio, and …

You’ve written a bunch of clever code to handle all the differences.

But… how do you know which piece of clever code to run ?

This is a tiny package whose only purpose is to let you detect which
async library your code is running under.

Examples
==============

.. code-block:: python
   :linenos:


    from sniffio import current_async_library

    async def generic_sleep(seconds):
        library = current_async_library()
        if library == "trio":
            import trio
            await trio.sleep(seconds)
        elif library == "asyncio":
            import asyncio
            await asyncio.sleep(seconds)
        # ... and so on ...
        else:
            raise RuntimeError(f"Unsupported library {library!r}")

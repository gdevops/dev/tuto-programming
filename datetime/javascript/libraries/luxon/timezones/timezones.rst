.. index::
   pair:  timezones ; luxon
   ! luxon timezones

.. _luxon_timezones:

========================================================================
luxon **timezones**
========================================================================

.. seealso::

   - https://moment.github.io/luxon/docs/manual/tour.html#time-zones
   - https://moment.github.io/luxon/docs/manual/zones.html




Timezones
===========

Luxon supports time zones. There's a `whole big section about it`_.

But briefly, you can create DateTimes in specific zones and change
their zones

::

    DateTime.fromObject({zone: 'America/Los_Angeles'}) // now, but expressed in LA's local time
    DateTime.local().setZone('America/Los_Angeles') // same

Luxon also supports UTC directly::

    DateTime.utc(2017, 5, 15);
    DateTime.utc();
    DateTime.local().toUTC();
    DateTime.utc().toLocal();


.. _`whole big section about it`:  https://moment.github.io/luxon/docs/manual/zones.html

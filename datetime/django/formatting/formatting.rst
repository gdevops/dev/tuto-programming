.. index::
   pair: datetime ; Formatting

.. _django_formatting_1:

============================
Django datetime formatting
============================


.. toctree::
   :maxdepth: 3

   i18n/i18n
   template/template

.. index::
   pair: Javascript ; temporal
   ! temporal

.. _temporal:

=====================================
Javascript **temporal**
=====================================

.. seealso::

   - :ref:`esnext_temporal`
   - https://github.com/tc39/proposal-temporal/blob/main/README.md
   - https://github.com/tc39/proposal-temporal
   - https://github.com/tc39/proposal-temporal/commits/main
   - https://tc39.es/proposal-temporal/docs/
   - https://tc39.es/proposal-temporal/docs/cookbook.html
   - https://github.com/tc39/proposal-temporal/tree/main/polyfill


.. toctree::
   :maxdepth: 3

   calendar/calendar
   cookbook/cookbook
   datetime/datetime
   duration/duration
   now/now
   timezones/timezones
   polyfill/polyfill

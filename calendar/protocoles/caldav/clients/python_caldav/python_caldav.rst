.. index::
   pair; Python; CalDAV

.. _python_caldav:

==================================================================
**Python CALDAV is a CalDAV (RFC4791) client library for Python**
==================================================================

.. toctree::
   :maxdepth: 3

   calendrier/calendrier
   tasks/tasks
   examples/examples

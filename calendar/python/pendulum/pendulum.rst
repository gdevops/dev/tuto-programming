.. index::
   pair: pendulum ; calendar
   pair: pendulum ; end_of

.. _pendulum_calendar:

=====================================
**pendulum**
=====================================

- :ref:`python_pendulum_library`


Use the end_of("month") method
==================================


If you use Django, tz=settings.TIME_ZONE


::
    # in base.py

    USE_TZ = True
    TIME_ZONE_EUROPE_PARIS = "Europe/Paris"
    TIME_ZONE = TIME_ZONE_EUROPE_PARIS

    # in an other Python module
    from django.conf import settings

    ...

    first_day = pendulum.datetime(2022, 9, 1, tz=settings.TIME_ZONE)


::

    import pendulum
    first_day = pendulum.datetime(2022, 9, 1, tz='Europe/Paris')
    last_day = first_day.end_of("month")
    day = first_day
    while (day.day <= last_day.day and day.month==first_day.month):
        print(f"{day=}")
        day = day.add(days=1)


::

    day=DateTime(2022, 9, 1, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 2, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 3, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 4, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 5, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 6, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 7, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 8, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 9, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 10, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 11, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 12, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 13, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 14, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 15, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 16, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 17, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 18, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 19, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 20, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 21, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 22, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 23, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 24, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 25, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 26, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 27, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 28, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 29, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    day=DateTime(2022, 9, 30, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))

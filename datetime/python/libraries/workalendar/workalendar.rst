.. index::
   pair: workalendar library; Python

.. _python_workalendar_library:

=======================================================================================
Python **workalendar library** (Worldwide holidays and workdays computational toolkit)
=======================================================================================


.. seealso::

   - https://github.com/peopledoc/workalendar
   - https://www.pycon.fr/2019/fr/talks/conference.html#workalendar%2C%20cette%20biblioth%C3%A8que%20qui%20ne%20prend%20jamais%20de%20vacances



Overview
============

Workalendar is a Python module that offers classes able to handle calendars,
list legal / religious holidays and gives working-day-related computation functions.

## Status

This library is ready for production, although we may warn eventual users:
some calendars may not be up-to-date, and this library doesn’t cover all the
existing countries on earth (yet).


.. index::
   pair: Django ; Async views

.. _django_async_views_prog:

=================================================
2020-03-18 **Django async views : it's done !**
=================================================

.. seealso::

   - :ref:`django_3_1_async_view`
   - :ref:`django_dep_0009`
   - https://github.com/django/django/commit/fc0fa72ff4cdbf5861a366e31cb8bbacd44da22d

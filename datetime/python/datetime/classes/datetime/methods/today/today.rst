.. index::
   pair: datetime class; today method

.. _python_datetime_class_today_method:

============================================
Python **datetime class** today() method()
============================================




classmethod datetime.today()
================================

Return the current local datetime, with tzinfo None.

Equivalent to:

datetime.fromtimestamp(time.time())


This method is functionally equivalent to now(), but without a tz parameter.

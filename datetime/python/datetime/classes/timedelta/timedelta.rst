.. index::
   pair: python ; timedelta
   pair: timedelta ; total_seconds()

.. _python_timedelta:

=====================================
Python **timedelta class**
=====================================

.. seealso::

   - https://docs.python.org/dev/library/datetime.html#timedelta-objects
   - :ref:`postgresql_interval_field`
   - :ref:`models_duration_field`




Description
============

A timedelta object represents a duration, the difference between two dates or times.


.. code-block:: python
   :linenos:


    class datetime.timedelta(days=0, seconds=0, microseconds=0, milliseconds=0, minutes=0, hours=0, weeks=0)
        """
        All arguments are optional and default to 0. Arguments may be integers or floats, and may be positive or negative.

        Only days, seconds and microseconds are stored internally. Arguments are converted to those units:

            A millisecond is converted to 1000 microseconds.

            A minute is converted to 60 seconds.

            An hour is converted to 3600 seconds.

            A week is converted to 7 days.

        and days, seconds and microseconds are then normalized so that the representation is unique, with

            0 <= microseconds < 1000000

            0 <= seconds < 3600*24 (the number of seconds in one day)

            -999999999 <= days <= 999999999
        """


The following example illustrates how any arguments besides days, seconds
and microseconds are “merged” and normalized into those three resulting attributes::

    >>> from datetime import timedelta
    >>> delta = timedelta(
    ...     days=50,
    ...     seconds=27,
    ...     microseconds=10,
    ...     milliseconds=29000,
    ...     minutes=5,
    ...     hours=8,
    ...     weeks=2
    ... )

::

    >>> # Only days, seconds, and microseconds remain
    >>> delta

::

    datetime.timedelta(days=64, seconds=29156, microseconds=10)



timedelta.total_seconds()
============================

Return the total number of seconds contained in the duration.
Equivalent to td / timedelta(seconds=1).

For interval units other than seconds, use the division form directly (e.g. td / timedelta(microseconds=1)).

Note that for very large time intervals (greater than 270 years on most platforms)
this method will lose microsecond accuracy.

New in version 3.2.


Example
===========

::


        # ancien nom sybase: temps
        temps_impute = models.DurationField(
            default=timedelta(hours=0),
            help_text=_("Le temps impute"),
            verbose_name=_("Temps imputé"),
        )
        commentaire = models.TextField(
            default="", help_text=_("Commentaire"), verbose_name=_("Commentaire")
        )
        etat = models.PositiveSmallIntegerField(
            choices=FICHE_STATUS,
            # Valeur par défaut
            default=FICHE_STATUS.FICHE_MODIFIABLE,
            help_text=_("indique l'état de la fiche"),
            verbose_name=_("Etat fiche"),
        )

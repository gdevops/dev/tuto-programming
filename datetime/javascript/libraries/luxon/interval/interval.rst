.. index::
   pair:  interval ; luxon
   ! luxon interval

.. _luxon_interval:

========================================================================
luxon **interval**
========================================================================

.. seealso::

   - https://moment.github.io/luxon/docs/class/src/interval.js~Interval.html




Interval
===========

An Interval object represents a half-open interval of time, where each
endpoint is a DateTime.

Conceptually, it's a container for those two endpoints, accompanied by
methods for creating, parsing, interrogating, comparing, transforming,
and formatting them.

Here is a brief overview of the most commonly used methods and getters in Interval:

- Creation To create an Interval, use fromDateTimes, after, before, or fromISO.
- Accessors Use start and end to get the start and end.
- Interrogation To analyze the Interval, use count, length, hasSame,
  contains, isAfter, or isBefore.
- Transformation To create other Intervals out of this one, use set,
  splitAt, splitBy, divideEqually, merge, xor, union, intersection, or difference.
- Comparison To compare this Interval to another one, use equals, overlaps,
  abutsStart, abutsEnd, engulfs
- Output To convert the Interval into other representations, see toString,
  toISO, toISODate, toISOTime, toFormat, and toDuration.


.. index::
   pair: Python asynchronous ; Libraries

.. _python_async_lib:

===========================================
Python asynchronous programming libraries
===========================================

.. seealso::

   - https://github.com/florimondmanca/awesome-asgi
   - https://github.com/timofurrer/awesome-asyncio
   - https://docs.python.org/3/library/asyncio-api-index.html
   - https://github.com/aio-libs


.. toctree::
   :maxdepth: 3

   anyio/anyio
   asyncio/asyncio
   sniffio/sniffio
   trio/trio

.. index::
   pair: pendulum; why
   pair: milliseconds; pendulum
   pair: diff; pendulum
   pair: timezone-aware ; datetimes

.. _pendulum_why:

===========================================
pendulum why ? (timezone-aware datetimes)
===========================================

- https://github.com/sdispater/pendulum


Why Pendulum ?
=================

Native datetime instances are enough for basic cases but when you face more
complex use-cases they often show limitations and are not so intuitive to
work with.

Pendulum provides a cleaner and more easy to use API while still relying on
the standard library. **So it's still datetime but better**.

Unlike other datetime libraries for Python, Pendulum is a drop-in replacement
for the standard datetime class (it inherits from it), so, basically, you can
replace all your datetime instances by DateTime instances in you code (exceptions
exist for libraries that check the type of the objects by using the type
function like sqlite3 or PyMySQL for instance).

It also **removes the notion of naive datetimes**: each Pendulum instance
is **timezone-aware** and by default in UTC for ease of use.

Pendulum also improves the standard timedelta class by providing more intuitive
methods and properties.

Display "YYYY-MM-DD"
---------------------------

::

    print(pendulum.now("Europe/Paris").format("YYYY-MM-DD"))

::

    In [1]: import pendulum
    In [2]: print(pendulum.now("Europe/Paris").format("YYYY-MM-DD"))

::

    2023-10-05


Display in french with milliseconds
--------------------------------------

::

    >>> print(pendulum.now("Europe/Paris").format("dddd DD MMMM YYYY HH:mm:ss:SSS", locale="fr"))

::

    jeudi 16 février 2023 16:10:01:323


Display YYY-MM-DD with milliseconds
--------------------------------------

::

    >>> print(pendulum.now("Europe/Paris").format("YYYY-MM-DD HH:mm:ss:SSS"))

::

    2023-02-16 16:12:43:864


Display diff time
---------------------

.. code-block:: python

    begin: pendulum.DateTime = None

    def log_request(request):
        global begin
        begin = pendulum.now("Europe/Paris")
        begin_str = begin.format("HH:mm:ss:SSS")
        print(
            f"\nRequest event hook:\n{begin_str} {request.method} {request.url}\nheaders:{request.headers}\nWaiting for response"
        )


    def log_response(response):
        end = pendulum.now("Europe/Paris")
        end_str = end.format("HH:mm:ss:SSS")
        diff = end - begin
        milliseconds = diff.microseconds/1000
        request = response.request
        print(
            f"\nResponse event hook:\n{end_str} (in {milliseconds} ms) {request.method} {request.url}\nStatus:{response.status_code}\n"
        )


::

    >>> begin = pendulum.now("Europe/Paris")
    >>> end = pendulum.now("Europe/Paris")
    >>> diff = end - begin

::

    >>> diff.
    diff.as_interval()      diff.hours              diff.in_months()        diff.in_years()         diff.min                diff.remaining_days     diff.start              diff.total_seconds(
    diff.as_timedelta()     diff.in_days()          diff.in_seconds()       diff.invert             diff.minutes            diff.remaining_seconds  diff.total_days()       diff.total_weeks()
    diff.days               diff.in_hours()         diff.in_weeks()         diff.max                diff.months             diff.resolution         diff.total_hours()      diff.weeks
    diff.end                diff.in_minutes()       diff.in_words(          diff.microseconds       diff.range(             diff.seconds            diff.total_minutes()    diff.years

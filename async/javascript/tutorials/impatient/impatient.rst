
.. index::
   pair: Javascript asynchronous ; Tutorials

.. _async_tuto_rauschma:

======================================================================
**Tuto Asynchronous programming in JavaScript** by Axel Rauschmayer
======================================================================

.. seealso::

   - https://exploringjs.com/impatient-js/ch_async-js.html
   - https://x.com/rauschma





A roadmap for asynchronous programming in JavaScript
=========================================================

This section provides a roadmap for the content on asynchronous programming
in JavaScript.

Don’t worry about the details!

Don’t worry if you don’t understand everything yet.

This is just a quick peek at what’s coming up.

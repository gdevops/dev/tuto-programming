.. index::
   ! JSCalendar

.. _jscalendar:

================================================
JSCalendar
================================================


- https://www.rfc-editor.org/info/rfc8984
- https://datatracker.ietf.org/doc/html/draft-ietf-calext-jscalendar
- https://www.bortzmeyer.org/8984.html

.. toctree::
   :maxdepth: 6

   description/description
   clients/clients

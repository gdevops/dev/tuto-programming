.. index::
   pair: models ; Django

.. _django_models_date_1:

============================
Django models
============================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/models
   - - https://github.com/django/django/blob/master/django/db/models

.. toctree::
   :maxdepth: 3

   datetimefield/datetimefield
   durationfield/durationfield

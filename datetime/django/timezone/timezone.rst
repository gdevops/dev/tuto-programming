
.. index::
   pair: Timezone ; Django

.. _django_timezone:

============================
Django **Timezone**
============================

.. seealso::

   - https://docs.djangoproject.com/fr/2.2/topics/i18n/timezones/
   - https://docs.djangoproject.com/fr/2.2/topics/i18n/formatting/





Overview
==========

When support for time zones is enabled, Django stores datetime information in
UTC in the database, uses time-zone-aware datetime objects internally, and
translates them to the end user’s time zone in templates and forms.

This is handy if your users live in more than one time zone and you want to
display datetime information according to each user’s wall clock.

**Even if your website is available in only one time zone**, it’s still good
practice to store data in UTC in your database.

**The main reason is Daylight Saving Time (DST)**.

Many countries have a system of DST, where clocks are moved forward in spring
and backward in autumn. If you’re working in local time, you’re likely to
encounter errors twice a year, when the transitions happen.
(The pytz documentation discusses these issues in greater detail.)

This probably doesn’t matter for your blog, but it’s a problem if you over-bill
or under-bill your customers by one hour, twice a year, every year.

The solution to this problem is to use UTC in the code and use local time only
when interacting with end users.

Time zone support is disabled by default.
To enable it, set USE_TZ = True in your settings file.

Time zone support uses pytz, which is installed when you install Django.


.. note::  The default settings.py file created by django-admin startproject
   includes USE_TZ = True for convenience.

.. note:: There is also an independent but related USE_L10N setting that
   controls whether Django should activate format localization.

   See `Format localization`_ for more details.

   If you’re wrestling with a particular problem, start with the time zone FAQ.


.. _`Format localization`: https://docs.djangoproject.com/fr/2.2/topics/i18n/formatting/



Example
=========


With TIME_ZONE = "America/Bogota", we can see that we have the offset
of 5 hours (05:00:00) and 4h59'59" (04:59:59).


::

    liste_des_transactions = LogTransactionNotariale.objects.filter(
        id_etude_notariale=agence.id_etude_notariale,
        date_transaction__range=(from_date, to_date),
    )
    logger.info(
        f"\n{from_date=} {to_date=}\nSQL query:\n{str(liste_des_transactions.query)}"
    )

::

    container_django_transactions_colombie_dev | from_date=DateTime(2020, 3, 3, 0, 0, 0, 3025, tzinfo=Timezone('America/Bogota'))
                                                 to_date=DateTime(2020, 4, 3, 23, 59, 59, 4065, tzinfo=Timezone('America/Bogota'))
    container_django_transactions_colombie_dev |
    SQL query
    SELECT `log_transaction_notariale`.`id`, `log_transaction_notariale`.`id_etude_notariale`, `log_transaction_notariale`.`date_transaction`,
           `log_transaction_notariale`.`agence_id`, `log_transaction_notariale`.`status_transaction`, `log_transaction_notariale`.`finger1`,
           `log_transaction_notariale`.`finger2`, `log_transaction_notariale`.`match_local`, `log_transaction_notariale`.`match_rnec1`,
           `log_transaction_notariale`.`match_rnec2`, `log_transaction_notariale`.`match_biographics`,
           `log_transaction_notariale`.`global_match_rnec`, `log_transaction_notariale`.`match_rnec_enabled`,
           `log_transaction_notariale`.`match_local_enabled`
    FROM `log_transaction_notariale` WHERE `log_transaction_notariale`.`date_transaction`
    BETWEEN 2020-03-03T05:00:00.003025 AND 2020-04-04T04:59:59.004065
    ORDER BY `log_transaction_notariale`.`date_transaction` DESC

.. index::
   pair: dateparse ; Django

.. _meta_django_dateparse:

===========================================================
Django utils **dateparse.py**  + **tests_dateparse.py**
===========================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/utils/dateparse.py
   - https://github.com/django/django/blob/master/tests/utils_tests/test_dateparse.py


.. toctree::
   :maxdepth: 3

   dateparse/dateparse
   test_dateparse/test_dateparse

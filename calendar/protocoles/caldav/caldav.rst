.. index::
   ! CalDAV

.. _caldav:

================================================
**CalDAV (Calendaring Extensions to WebDAV)**
================================================


- https://fr.wikipedia.org/wiki/CalDAV

.. toctree::
   :maxdepth: 6

   description/description
   clients/clients
   servers/servers


.. index::
   pair: date ; querysets
   pair: datetime ; casts the value as date

.. _querysets_date:

===================================================================
querysets date (For datetime fields, **casts the value as date**)
===================================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/models/querysets/#date




date
=====

For datetime fields, casts the value as date. Allows chaining additional field
lookups. Takes a date value.

Example::

    Entry.objects.filter(pub_date__date=datetime.date(2005, 1, 1))
    Entry.objects.filter(pub_date__date__gt=datetime.date(2005, 1, 1))

(No equivalent SQL code fragment is included for this lookup because implementation
of the relevant query varies among different database engines.)

When USE_TZ is True, fields are converted to the current time zone before filtering.
This requires time zone definitions in the database.

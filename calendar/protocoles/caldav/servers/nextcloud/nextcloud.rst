.. index::
   pair: Calendar; Nextcloud

.. _nextcloud_calendar:

==========================================================================
**Nextcloud calendar**
==========================================================================


En français
==============

- https://docs.nextcloud.com/server/latest/user_manual/fr/groupware/calendar.html


English translation
=====================

- https://docs.nextcloud.com/server/latest/user_manual/en/groupware/calendar.html


Utiliser l’application Agenda
================================

- https://docs.nextcloud.com/server/latest/user_manual/fr/groupware/calendar.html#using-the-calendar-app


Éditer, exporter ou supprimer un Calendrier
=============================================

- https://docs.nextcloud.com/server/latest/user_manual/fr/groupware/calendar.html#edit-export-or-delete-a-calendar

Parfois vous désirez changer la couleur ou le nom d’un agenda déjà
importé ou créé.

Vous pouvez aussi vouloir l’exporter vers votre disque local ou le
supprimer de manière définitive


.. note:: Noter que la suppression d’un agenda est une action irréversible.
   Après sa suppression, il n’y a aucun moyen de restaurer l’agenda à
   moins que vous disposiez d’une sauvegarde locale.



.. _agendas_partages:

Agendas partagés
==================

- https://docs.nextcloud.com/server/latest/user_manual/fr/groupware/calendar.html#sharing-calendars

**Vous pouvez partager votre agenda avec d’autres utilisateurs ou groupes**.

Les agendas peuvent être partagés en **ecriture ou en lecture seule**.

Lors d’un partage en écriture, les utilisateurs avec lesquels l’agenda
est partagé auront la possibilité de créer de nouveaux événements, de
modifier ou supprimer des événements déjà existants.


.. note:: Le partage de calendrier ne peut pour l’instant pas être accepté
   ou décliné.
   Si vous souhaitez faire disparaître un agenda qui vous a été partagé,
   vous pouvez cliquer sur le bouton de menu à côté de l’agenda et cliquer
   sur « Cesser le partage avec moi ».

   Les agendas partagés avec un groupe ne peuvent pas être retirés de la
   liste par les utilisateurs.



.. _publier_un_agenda:

Publier un agenda
====================

Les agendas peuvent être publiés via un lien public pour les rendre accessibles
(en lecture seule) à des utilisateurs externes.

Vous pouvez créer un lien public en ouvrant le menu Partage d’agenda et
cliquer sur « + » en face de **Lien de Partage**.

Une fois créé, vous pouvez copier le lien public dans votre presse-papier
ou l’envoyer par mail.

Il y a aussi un « code d’insertion » qui fournit un iframe HTML pour
insérer votre calendrier dans des pages publics

Plusieurs agendas peuvent être partagés en ajoutant leur identifiant
unique à la fin d’un lien interne.

**L’identifiant unique peut être trouvé à la fin du lien de partage de
chaque agenda**.
L’adresse complète ressemble à **https://cloud.exemple.com/index.php/apps/agenda/lien interne/1 - 2 -3**

Pour changer la vue par défaut ou la date d’un agenda intégré, vous devez
fournir un URL qui ressemble à https://cloud.example.com/index.php/apps/calendar/embed/<token>/<view>/<date>.

Dans cette URL vous devez remplacer les variables suivantes :

- <token> avec le jeton du calendrier
- <view> avec un des paramètres suivants:
  - dayGridMonth,
  - timeGridWeek,
  - timeGridDay,
  - listMonth,
  - listWeek,
  - listDay.

  La vue par défaut est dayGridMonth et la liste utilisée habituellement
  est listMonth,

- «1» avec «maintenant» ou toute autre date avec le format suivant
  « 2 - 3 - 4 «  (par ex. « 2019-12-28 « ).

Sur la page publique, les utilisateurs sont en mesure de récupérer le
lien d’inscription à l’agenda et de récupérer le calendrier entier directement.


.. _abonnement_agenda:

S’abonner à un agenda
=========================

Vous pouvez accéder à des agendas Ical dans Nextcloud.

Par le support du protocole d’interopérabilité (RFC 5545) nous assurons
la compatibilité du calendrier Nextcloud avec ceux de Google d’Apple Icloud
et bien d’autres calendriers de serveurs, de sorte que vous pouvez faire
des échanges entre calendriers y compris celui de liens d’un agenda publié
sur d’autre instances de Nextcloud, comme décrit ci-dessus.

- Cliquer sur « + Nouvel abonnement «  dans le panneau latéral à gauche.
- Saisir ou coller **le lien** d’un agenda partagé auquel vous voulez vous abonner.

Terminé. Vos abonnements à un agenda seront mis à jour de manière régulière.

.. note:: Les abonnements sont rafraichis chaque semaine par défaut.
   Votre adminstrateur peut changer ce paramétrage

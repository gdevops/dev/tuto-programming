.. index::
   pair:  duration ; luxon
   ! luxon duration

.. _luxon_duration:

========================================================================
luxon **duration**
========================================================================

.. seealso::

   - https://moment.github.io/luxon/docs/class/src/duration.js~Duration.html
   - https://en.wikipedia.org/wiki/ISO_8601#Durations




Interval
===========

A Duration object represents a period of time, like "2 months" or "1 day,
1 hour".

Conceptually, it's just a map of units to their quantities, accompanied
by some additional configuration and methods for creating, parsing,
interrogating, transforming, and formatting them.

They can be used on their own or in conjunction with other Luxon types;
for example, you can use DateTime.plus to add a Duration object to a
DateTime, producing another DateTime.

Here is a brief overview of commonly used methods and getters in Duration:

- Creation To create a Duration, use Duration.fromMillis, Duration.fromObject,
  or Duration.fromISO.
- Unit values See the Duration.years, Duration.months, Duration.weeks,
  Duration.days, Duration.hours, Duration.minutes, Duration.seconds,
  Duration.milliseconds accessors.
- Configuration See Duration.locale and Duration.numberingSystem accessors.
- Transformation To create new Durations out of old ones use Duration.plus,
  Duration.minus, Duration.normalize, Duration.set, Duration.reconfigure,
  Duration.shiftTo, and Duration.negate.
- Output To convert the Duration into other representations,
  see Duration.as, Duration.toISO, Duration.toFormat, and Duration.toJSON

There's are more methods documented below. In addition, for more information
on subtler topics like internationalization and validity, see the external
documentation.


.. index::
   pair: python ; chore
   ! python-chore

.. _python_chore:

=====================================================================================================================================================================================================
**python-chore: A multi-platform job dispatcher to help support multiple cluster and job scheduling platforms at the same time. Job managers such as slurm, lsf and good old bash are supported**
=====================================================================================================================================================================================================

- https://pypi.org/project/python-chore/
- https://gitlab.com/doctormo/python-chore/


Description
==============

A multi-platform job dispatcher to help support multiple cluster and job
scheduling platforms at the same time. Job managers such as slurm, lsf
and good old bash are supported.

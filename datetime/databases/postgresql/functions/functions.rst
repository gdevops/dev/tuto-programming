
.. index::
   pair: Postgresql ; Datetime Programming

.. _postgresql_datetime_functions__programming:

=====================================
Postgresql datetime programming
=====================================

.. seealso::

   - https://www.postgresql.org/docs/devel/functions-datetime.html
   - https://github.com/django/django/blob/master/django/db/models/functions/datetime.py





Date/Time Functions and Operators
=====================================

.. seealso::

   - https://www.postgresql.org/docs/devel/functions-datetime.html


.. _python_caldav_examples:

==================================================================
**Examples**
==================================================================

- https://github.com/tobixen/calendar-cli
- https://github.com/python-caldav/caldav/blob/master/examples/scheduling_examples.py
- https://github.com/python-caldav/caldav/blob/master/tests/test_caldav.py
- https://github.com/tobixen


Check the examples folder, particularly basic examples.

There is also a `scheduling examples <https://github.com/python-caldav/caldav/blob/master/examples/scheduling_examples.py>`_ for sending, receiving and replying
to invites, though this is not very well-tested so far.

The test code also covers lots of stuff, though it’s not much optimized
for readability (at least not as of 2020-05).

`Tobias Brox <https://github.com/tobixen>`_ is also working on a `command line interface <https://github.com/tobixen/calendar-cli>`_
built around the caldav library.



Usage Examples
==================

.. _caldav_basic_usage_examples:

basic_usage_examples
-------------------------

- https://github.com/python-caldav/caldav/blob/master/examples/basic_usage_examples.py


.. literalinclude::  examples.py
   :linenos:

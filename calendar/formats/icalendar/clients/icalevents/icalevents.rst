.. index::
   pair: python ; icalevents
   ! icalevents

.. _python_icalevents:

==========================================================================
**The icalevents Python module for iCal URL/file parsing and querying**
==========================================================================


- https://github.com/irgangla/icalevents




Description
=============

Simple Python 3 library to download, parse and query iCal sources.


.. index::
   pair: Python asynchronous ; Trio

.. _trio:

==================================================================
Trio (a friendly Python library for async concurrency and I/O)
==================================================================

.. seealso::

   - https://github.com/python-trio/trio
   - https://trio.readthedocs.io/en/latest/
   - https://x.com/vorpalsmith

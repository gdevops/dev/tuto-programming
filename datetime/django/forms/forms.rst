.. index::
   pair: forms ; Django

.. _django_forms_date_1:

============================
Django forms
============================

.. seealso::

   - https://docs.djangoproject.com/en/2.2/ref/forms/fields
   - https://github.com/django/django/blob/master/django/forms


.. toctree::
   :maxdepth: 3

   datetimefield/datetimefield
   durationfield/durationfield

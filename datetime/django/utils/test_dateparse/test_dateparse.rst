
.. _test_dateparse:

===========================================================
**tests/utils_tests/test_dateparse.py**
===========================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/utils/dateparse.py
   - https://github.com/django/django/blob/master/tests/utils_tests/test_dateparse.py





::

    $ find . -name "*dateparse.py" -print

::

    ./tests/utils_tests/test_dateparse.py
    ./django/utils/dateparse.py



tests/utils_tests/test_dateparse.py
======================================

.. literalinclude:: test_dateparse.py
   :linenos:


class DurationParseTests(unittest.TestCase)
============================================

.. literalinclude:: tests_parse_duration/tests_parse_duration.py
   :linenos:

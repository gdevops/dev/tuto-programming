
.. _programming_people:

=====================
People
=====================

.. toctree::
   :maxdepth: 3

   andrew_chael/andrew_chael
   frances_allen/frances_allen
   katie_bouman/katie_bouman

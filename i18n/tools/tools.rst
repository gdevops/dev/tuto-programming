.. index::
   pair: i18n ; Tools


.. _i18n_tools:

==========================
**Tools**
==========================

.. toctree::
   :maxdepth: 3

   strike/strike
   utf8_xyz/utf8_xyz

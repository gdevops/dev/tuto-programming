


.. _inspired_pathlib:

============================================================================================
Common Path Patterns + **Python Pattern Matching** Examples: Working with Paths and Files
============================================================================================


- https://www.inspiredpython.com/article/common-path-patterns
- https://www.inspiredpython.com/course/pattern-matching/python-pattern-matching-examples-working-with-paths-and-files

.. index::
   pair:  Versions ; Luxon

.. _luxon_versions:

==================
luxon versions
==================

.. seealso::

   - https://github.com/moment/luxon/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   1.24.1/1.24.1
   1.0.0/1.0.0

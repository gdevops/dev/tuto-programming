.. index::
   pair: Datetime ; Forms Field

.. _forms_datetime_field:

============================
Forms DateTime field
============================

.. seealso::

   - https://docs.djangoproject.com/en/2.2/ref/forms/fields/#datetimefield
   - https://github.com/django/django/blob/master/django/forms/fields.py
   - https://github.com/django/django/blob/master/django/forms/widgets.py





DateTimeField
===============

class DateTimeField(**kwargs**)
---------------------------------

- Default widget: DateTimeInput
- Empty value: None
- Normalizes to: A Python **datetime.datetime** object.
- Validates that the given value is either a datetime.datetime, datetime.date
  or string formatted in a particular datetime format.
- Error message keys: required, invalid


.. code-block:: python

    class DateTimeField(BaseTemporalField):
        widget = DateTimeInput
        input_formats = formats.get_format_lazy('DATETIME_INPUT_FORMATS')
        default_error_messages = {
            'invalid': _('Enter a valid date/time.'),
        }

        def prepare_value(self, value):
            if isinstance(value, datetime.datetime):
                value = to_current_timezone(value)
            return value

        def to_python(self, value):
            """
            Validate that the input can be converted to a datetime. Return a
            Python datetime.datetime object.
            """
            if value in self.empty_values:
                return None
            if isinstance(value, datetime.datetime):
                return from_current_timezone(value)
            if isinstance(value, datetime.date):
                result = datetime.datetime(value.year, value.month, value.day)
                return from_current_timezone(result)
            result = super().to_python(value)
            return from_current_timezone(result)

        def strptime(self, value, format):
            return datetime.datetime.strptime(value, format)


DateTimeInput
==============

.. seealso::

   - https://github.com/django/django/blob/a5855c8f0fe17b7e888bd8137874ef78012a7294/django/forms/widgets.py#L492


::

    class DateTimeBaseInput(TextInput):
        format_key = ''
        supports_microseconds = False

        def __init__(self, attrs=None, format=None):
            super().__init__(attrs)
            self.format = format or None

        def format_value(self, value):
            return formats.localize_input(value, self.format or formats.get_format(self.format_key)[0])


    class DateTimeInput(DateTimeBaseInput):
        format_key = 'DATETIME_INPUT_FORMATS'
        template_name = 'django/forms/widgets/datetime.html'



django/forms/widgets/datetime.html
====================================

.. seealso::

   - https://raw.githubusercontent.com/django/django/master/django/forms/templates/django/forms/widgets/datetime.html


::

    {% include "django/forms/widgets/input.html" %}


django/forms/widgets/input.html
=================================

.. seealso::

   - https://raw.githubusercontent.com/django/django/master/django/forms/templates/django/forms/widgets/input.html

::

    <input type="{{ widget.type }}" name="{{ widget.name }}"
    {% if widget.value != None %} value="{{ widget.value|stringformat:'s' }}"{% endif %}
    {% include "django/forms/widgets/attrs.html" %}>


django/forms/widgets/attrs.html
==================================

.. seealso::

   - https://raw.githubusercontent.com/django/django/master/django/forms/templates/django/forms/widgets/attrs.html


::

    {% for name, value in widget.attrs.items %}{% if value is not False %} {{ name }}
    {% if value is not True %}="{{ value|stringformat:'s' }}"{% endif %}{% endif %}{% endfor %}



ISODateTimeField
=================

.. seealso::

   - https://code.djangoproject.com/ticket/11385

::


    from django.utils.dateparse import parse_datetime
    from django.utils.encoding import force_str


    class ISODateTimeField(forms.DateTimeField):

        def strptime(self, value, format):
            return parse_datetime(force_str(value))

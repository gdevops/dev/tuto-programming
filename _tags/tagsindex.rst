:orphan:

.. _tagoverview:

Tags overview
#############

.. toctree::
    :caption: Tags
    :maxdepth: 1

    Bloatware (1) <bloatware.rst>
    Nilklaus Wirth (1) <nilklaus-wirth.rst>

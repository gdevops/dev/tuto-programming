.. index::
   pair: Calendar; Protocoles
   pair: Edition; Protocoles


.. _calendar_protocoles:

================================
Calendar edition protocoles
================================


.. toctree::
   :maxdepth: 2

   caldav/caldav
   web_calendar_access_protocol/web_calendar_access_protocol


.. index::
   pair: f-string ; realpython

.. _f_string_realpython:

=========================================================================
Python 3's **f-Strings**: An Improved String Formatting Syntax (Guide)
=========================================================================

.. seealso::

   - https://realpython.com/python-f-strings/



Introduction
==============

As of Python 3.6, **f-strings** are a great new way to format strings.

Not only are they **more readable, more concise, and less prone to error**
than other ways of formatting, but **they are also faster !**

By the end of this article, you will learn how and why to start using
f-strings today.

But first, here’s what life was like before f-strings, back when you
had to walk to school uphill both ways in the snow.

.. index::
   ! temporal polyfill

.. _temporal_polyfill:

=====================================
Javascript **temporal** polyfill
=====================================

.. seealso::

   - https://github.com/tc39/proposal-temporal/blob/main/polyfill



Polyfill
==========

A complete polyfill can be found here_.

It is being developed as specification in code.

When viewing the reference documentation, the polyfill is automatically
loaded in your browser, so you can try it out by opening your browser's
developer tools.

.. _here: https://github.com/tc39/proposal-temporal/blob/main/polyfill

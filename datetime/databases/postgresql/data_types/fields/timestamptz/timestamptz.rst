.. index::
   pair: Postgresql ; timestamptz field
   pair: UTC ; timestamptz field
   pair: DateStyle ; Postgresql


.. _postgresql_timestamptz:

=====================================
Postgresql DDL **timestamptz field**
=====================================


.. seealso::

   - http://www.postgresqltutorial.com/postgresql-timestamp/






Example
========

::

    CREATE TABLE public.fiche_temps (
	    id serial NOT NULL,
	    temps_impute interval NOT NULL,
	    etat int2 NOT NULL,
	    created timestamptz NOT NULL,
	    modified timestamptz NULL,
	    id_employe int4 NOT NULL,
	    id_projet int4 NOT NULL,
	    commentaire text NOT NULL,
	    CONSTRAINT fiche_temps_etat_check CHECK ((etat >= 0)),
	    CONSTRAINT fiche_temps_pkey PRIMARY KEY (id),
	    CONSTRAINT unique_employe_projet_created UNIQUE (id_employe, id_projet, created)
    );
    CREATE INDEX employe_projet_created_index ON public.fiche_temps USING btree (id_employe, id_projet, created);
    CREATE INDEX fiche_temps_id_employe_cee33221 ON public.fiche_temps USING btree (id_employe);
    CREATE INDEX fiche_temps_id_projet_971aea17 ON public.fiche_temps USING btree (id_projet);



Dalibo Definition
=====================

Le type timestamp permet d’exprimer **une date et une heure**.

Par défaut, il ne connaît pas la notion de fuseau horaire. Lorsque le type est
déclaré timestamp with time zone, il est adapté aux conversions d’heure d’un
fuseau horaire vers un autre car le changement de date sera répercuté dans la
composante date du type de données. Il est précis à la microseconde.

Le format de saisie et de restitution des dates et heures dépend du paramètre
**DateStyle**.

La documentation de ce paramètre permet de connaître les différentes valeurs possibles.
Il reste néanmoins recommandé d’utiliser les fonctions de formatage de date qui
permettent de rendre l’application indépendante de la configuration du SGBD.

La norme ISO (ISO-8601) impose le format de date **année-mois-jour**

La norme SQL est plus permissive et permet de restituer une date au format
**jour/mois/année** si DateStyle est égal à *SQL, DMY*



Diomitri Fontaine Definition
==============================

PostgreSQL implements an interval data type along with the time, date and
timestamptz data types.

An interval describes **a duration, like a month or two weeks, or even a millisecond**.

set intervalstyle to postgres;

::

    select
        interval '1 month',
        interval '2 weeks',
        2 * interval '1 week',
        78389 * interval '1 ms' ;


Definition
============


The timestamptz data is the timestamp with time zone.

The timestamptz is a **time zone-aware date** and time data type.

PostgreSQL stores the timestamptz in UTC value. When you insert a value into a
timestamptz column, PostgreSQL converts the timestamptz value into a UTC value
and stores the UTC value in the table.

When you query timestamptz from the database, PostgreSQL converts the UTC value
back to the time value of the timezone set by the database server, the user,
or the current database connection.

Notice that both timestamp and timestamptz uses 8 bytes for storing the
timestamp value as shown in the following query:

::

    SELECT
          typname,
          typlen
    FROM
          pg_type
    WHERE
          typname ~ '^timestamp';


::

       typname   | typlen
    -------------+--------
     timestamp   |      8
     timestamptz |      8
    (2 rows)

It’s important to note that timestamptz value is stored as a UTC value.

PostgreSQL does not store any timezone data with the timestamptz value.

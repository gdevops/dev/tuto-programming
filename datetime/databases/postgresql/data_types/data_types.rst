.. index::
   pair: Postgresql ; DDL
   pair: Postgresql ; data types

.. _postgresql_ddl:

=====================================
Postgresql  **data types**
=====================================

.. seealso::

   - http://www.postgresqltutorial.com/postgresql-data-types/

.. toctree::
   :maxdepth: 3

   fields/fields

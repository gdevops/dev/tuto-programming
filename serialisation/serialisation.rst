.. index::
   ! serialisation


.. _serialsation:

==========================
**serialisation**
==========================

- https://rstockm.github.io/mastowall/?hashtags=python,serialisation&server=https://framapiaf.org


.. toctree::
   :maxdepth: 3


   definition/definition
   tutorials/tutorials
   python/python

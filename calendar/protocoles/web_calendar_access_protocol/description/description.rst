
================================================
**Web Calendar Access Protocol definition**
================================================


https://fr.wikipedia.org/wiki/Web_Calendar_Access_Protocol
=============================================================

**Web Calendar Access Protocol** est un protocole client-serveur d'accès à
distance aux calendriers et agendas basés sur les standards Internet XML,
HTTP, iCalendar et vCard.

WCAP a été créé pour être utilisé avec Sun Java Calendar Server, mais
est aussi utilisé par le projet open source Buni Meldware.

WCAP utilise de simples commandes HTTP GET pour accéder aux données iCalendar,
Freebusy, TODO et vCard. WCAP répond aussi bien sous forme de texte
traditionnel ou sous forme de « xml » iCalendar/etc2.

De nombreux plugins existent comme ceux pour Mozilla Thunderbird, Novell
Evolution et Microsoft Outlook.

Il existe un protocole concurrent nommé CalDAV en voie de standardisation.

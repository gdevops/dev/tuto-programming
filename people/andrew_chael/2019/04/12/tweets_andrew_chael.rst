

.. _tweets_andrew_chael_2019_04_12:

=========================
2019-04-12 Andrew Chael
=========================

- https://achael.github.io/
- https://github.com/achael/eht-imaging
- https://achael.github.io/eht-imaging/



https://x.com/thisgreyspirit/status/1116806744603336705
==================================================================


I just want to say thank you to the overwhelming number of people who have
said such kind words to me and the whole team.

It means a lot to see all the people out there who are excited about black hole
science and, more importantly, are decent humans.


https://x.com/thisgreyspirit/status/1116519313488470017
=================================================================

- https://x.com/thisgreyspirit/status/1116519313488470017
- https://github.com/achael/eht-imaging


(Also I did not write "850,000 lines of code" -- many of those "lines" tracked
by github are in model files.

There are about 68,000 lines in the current software, and I don't care how
many of those I personally authored)


https://x.com/thisgreyspirit/status/1116518544961830918 (1/7)
========================================================================


(1/7) So apparently some (I hope very few) people online are using the fact
that I am the primary developer of the eht-imaging software library
(link: https://github.com/achael/eht-imaging) to launch awful and sexist
attacks on my colleague and friend Katie Bouman. **Stop.**


https://x.com/thisgreyspirit/status/1116518545934962694 (2/7)
=======================================================================

(2/7) Our papers used three independent imaging software libraries
(including one developed by my friend @sparse_k).

While I wrote much of the code for one of these pipelines, Katie was a huge
contributor to the software; it would have never worked without her contributions
and

https://x.com/thisgreyspirit/status/1116518547327475712 (3/7)
=======================================================================


3/7) the work of many  others who wrote code, debugged, and figured out how to
use the code on challenging EHT data.

With a few others, Katie also developed the imaging framework that rigorously
tested all three codes and shaped the entire paper
(https://iopscience.iop.org/article/10.3847/2041-8213/ab0e85);


https://x.com/thisgreyspirit/status/1116518548552208384 (4/7)
========================================================================


(4/7) as a result, this is probably the most vetted image in the history of
radio interferometry.

I'm thrilled Katie is getting recognition for her work and that she's
inspiring people as an example of women's leadership in STEM.
I'm also thrilled she's pointing


https://x.com/thisgreyspirit/status/1116518549433012224 (5/7)
=======================================================================

(5/7) out that this was a team effort including contributions from many junior
scientists, including many women junior scientists
(https://www.facebook.com/photo.php?fbid=10213326021042929&set=a.10211451091290857&type=3&theater)

Together, we all make each other's work better; the number of commits doesn't
tell the full story of who was indispensable.


https://x.com/thisgreyspirit/status/1116518550297096194 (6/7)
========================================================================


(6/7) So while I appreciate the congratulations on a result that I worked hard
on for years, **if you are congratulating me because you have a sexist vendetta
against Katie, please go away and reconsider your priorities in life**.

Otherwise, stick around -- I hope to start tweeting


https://x.com/thisgreyspirit/status/1116518551291158528 (7/7)
======================================================================

7/7) more about black holes and other subjects I am passionate about --
including space, being a gay astronomer, Ursula K. Le Guin, architecture,
and musicals.
Thanks for following me, and let me know if you have any questions about
the EHT !


.. index::
   pair: datetime ; Programming

.. _datetime_programming:

==========================
Date {time,timedelta}
==========================

.. seealso::

   - http://boussejra.com/2018/07/04/timezones-are-hell.html

.. toctree::
   :maxdepth: 6

   databases/databases
   django/django
   python/python
   timezones/timezones
   javascript/javascript
   docker/docker

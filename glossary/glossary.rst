
.. index::
   ! Glossary

.. _glossary:

==========================
Glossary
==========================

.. glossary::

  Wirth's Law

      Software is getting slower more rapidly than hardware is becoming faster.

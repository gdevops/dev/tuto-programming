.. index::
   pair: serialisation; pickle


.. _pickle:

==========================
**pickle**
==========================

- https://rstockm.github.io/mastowall/?hashtags=python,pickle&server=https://framapiaf.org
- https://docs.python.org/3/library/pickle.html#comparison-with-json


.. index::
   pair: string ; Python

.. _python_string_programming:

==========================
Python string
==========================


.. toctree::
   :maxdepth: 3

   f_string/f_string

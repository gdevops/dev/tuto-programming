
.. index::
   pair: Timezone ; docker
   pair: Timezone ; /usr/share/zoneinfo


.. _article_timezone_docker_2020_04:

================================================
Timezone and docker (**/usr/share/zoneinfo**)
================================================

.. seealso::

   - :ref:`python_zoneinfo`




Source
========

.. seealso::

   - :ref:`jpetazzo_2020_04_07`
   - https://enix.io/fr/blog/cherie-j-ai-retreci-docker-part3/
   - https://www.ardanlabs.com/blog/2020/04/docker-images-part3-going-farther-reduce-image-size.html

Avant de conclure, quelques détails importants
===============================================

Lorsqu’on utilise des images vraiment très réduites (comme scratch, ou
d’une certaine manière alpine, ou même certaines images générées avec
distroless, Bazel ou Nix), on peut être confronté à des erreurs
inattendues.

Il y a des fichiers auxquels on ne pense pas en général, mais qui sont
supposés se trouver sur tout système UNIX bien élevé, y compris dans
un conteneur.

De quels fichiers parle-t-on exactement ? Et bien, voici une petite liste non exhaustive :

- les certificats TLS ;
- les fichiers de timezone ;
- la base d’utilisateurs et groupes (UID/GID).

Voyons de quoi il s’agit plus précisément, de quand nous pouvons en
avoir besoin, et comment on peut les ajouter à nos images.


Les fichiers de timezone
=============================

.. seealso::

   - https://x.com/sylr

Si notre code manipule la date et l’heure, en particulier l’heure locale
(par exemple, si on affiche l’heure dans un fuseau horaire particulier,
par opposition à une horloge interne), on a besoin des fichiers de
timezone.

Vous pourriez vous dire : “Attends, mais comment ça Jérôme ? Si je veux
manipuler les fuseaux horaires, j’ai seulement besoin d’un offset par
rapport à UTC ou GMT !”.

Oui, sauf que ... il y a les changements d’heure d'été et d’heure d’hiver.
Là, les choses se compliquent très vite, car différents pays ou régions
n’ont pas tous un passage à l’heure d'été, ou bien pas au même moment.

Par exemple, au sein même de l’Utah aux États-Unis, les règles changent
selon qu’on est en territoire Navajo ou pas !
Et puis par ailleurs, au fil des années, ces règles évoluent.
En Europe par exemple, le changement d’heure devrait être supprimé par
tous les pays en 2021.

Revenons-en à notre problématique technique.

Si on veut pouvoir afficher l’heure locale, on va avoir besoin des
fichiers qui décrivent ces informations. Sur UNIX, ce sont les fichiers
tzinfo ou zoneinfo, on les trouve généralement dans le répertoire **/usr/share/zoneinfo**.

Certaines images (par exemple centos ou debian) incluent nativement ces
fichiers de timezone.
D’autres non, comme alpine ou ubuntu, et le package qui inclut ces
fichiers s’appelle généralement tzdata.

Pour installer les fichiers de timezone dans notre image, on peut par
exemple lancer::

    COPY --from=debian /usr/share/zoneinfo /usr/share/zoneinfo

Ou si on utilise déjà alpine, on peut simplement faire un apk add tzdata.

Pour vérifier que les fichiers de timezone sont bien installés, on peut
faire cette commande dans notre conteneur::

    TZ=Europe/Paris date

Si on obtient quelque chose comme Fri Mar 13 21:03:17 CET 2020, on est
bon.

::

    $ TZ=Europe/Paris date

::


    dimanche 19 avril 2020, 11:35:01 (UTC+0200)


Si on obtient UTC, ça signifie que les fichiers de timezone n’ont pas
été trouvés.


Python and zoneinfo
======================

.. seealso::

   - :ref:`python_zoneinfo`

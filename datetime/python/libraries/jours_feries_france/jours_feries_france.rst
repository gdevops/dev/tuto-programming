.. index::
   pair: Jours fériés; France

.. _jours_feries_france:

================================================================
etalab/jours-feries-france
================================================================

- https://github.com/etalab/jours-feries-france


Description
=============

Librarie Python pour calculer les jours fériés en France

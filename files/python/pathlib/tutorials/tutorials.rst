
.. index::
   pair: pathlib ; Tutorials

.. _pathlib_tutorials:

==========================
pathlib tutorials
==========================

.. toctree::
   :maxdepth: 3


   pep_0428/pep_0428
   calmcode/calmcode
   inspired/inspired
   miguendes/miguendes
   realpython/realpython
   rednafi/rednafi
   treyhunner/treyhunner


.. _luxon_def:

==================================================================================
Luxon is a library for working with dates and times in Javascript
==================================================================================

.. seealso::

   - https://moment.github.io/luxon/docs/manual/why.html





Luxon is a library for working with dates and times in Javascript
===================================================================

.. code-block:: javascript

    DateTime.local().setZone('America/New_York').minus({ weeks: 1 }).endOf('day').toISO();


Origin
========

Luxon started because I had a bunch of ideas on how to improve Moment
but kept finding Moment wasn't a good codebase to explore them with.

Namely:

- I wanted to try out some ideas that I thought would provide a better,
  more explicit API but didn't want to break everything in Moment.
- I had an idea on how to provide out-of-the-box, no-data-files-required
  support for time zones, but Moment's design made that difficult.
- I wanted to completely rethink how internationalization worked by
  using the Intl API that comes packaged in browsers.
- I wanted to use a modern JS toolchain, which would require a major
  retrofit to Moment.

So I decided to write something from scratch, a sort of modernized Moment.

It's a combination of all the things I learned maintaining Moment and
Twix, plus a bunch of fresh ideas. I worked on it in little slivers of
spare time for about two years.

But now it's ready to actually use, and the Moment team likes it enough
that we pulled it under the organization's umbrella.

Ideas in Luxon
==================

Luxon is built around a few core ideas:

- Keep the basic chainable date wrapper idea from Moment.
- Make all the types immutable.
- Make the API explicit; different methods do different things and have
  well-defined options.
- Use the Intl API to provide internationalization, including token parsing.
  Fall back to English if the browser doesn't support those APIs.
- Abuse the Intl API horribly to **provide time zone support**.
  Only possible for modern browsers.
- Provide **more comprehensive duration support**.
- Directly provide interval support.
- Write inline docs for everything.

These ideas have some big advantages:

- It's much easier to understand and debug code that uses Luxon.
- Using **native browser capabilities for internationalization** leads
  to a much better behavior and is dramatically easier to maintain.
- Luxon has the **best time zone support of any JS date library**.
- **Luxon's durations** are both flexible and easy to use.
- The documentation is very good.

They also have some disadvantages:

- Using modern browser capabilities means that the fallback behavior
  introduces complexity for the programmer.
- Never keeping internationalized strings in the code base means that
  some capabilities have to wait until the browsers provide it.
- Some aspects of the Intl API are browser-dependent, which means Luxon's
  behavior is too.


Future plans
==============

Luxon is fully usable and I plan to support it indefinitely.

It's also largely complete.

It will certainly add relative time formatting (and an English-only fallback)
when that becomes possible.

Luxon will also eventually strip out its fallbacks for missing platform
features. But overall I expect the core functionality to stay basically
as it is, adding mostly minor tweaks and bugfixes.



Features
==========

- DateTime, Duration, and Interval types.
- Immutable, chainable, unambiguous API.
- Parsing and formatting for common and custom formats.
- **Native time zone and Intl support (no locale or tz files)**.

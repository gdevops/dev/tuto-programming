.. index::
   pair: isoweek library; Python

.. _python_isoweek_library:

================================================================
Python **isoweek library** (Objects representing a week )
================================================================


.. seealso::

   - https://github.com/gisle/isoweek
   - https://en.wikipedia.org/wiki/ISO_week_date




ISO Week
==========

The isoweek module provide the class Week. Instances represent specific weeks
spanning Monday to Sunday.

There are 52 or 53 numbered weeks in a year. Week 1 is defined to be the first
week with 4 or more days in January.

It's called isoweek because this is the week definition of ISO 8601.

This standard also define a notation for identifying weeks; yyyyWww (where the
"W" is a literal).

An example is "2011W08" which denotes the 8th week of year 2011.

Week instances stringify to this form.

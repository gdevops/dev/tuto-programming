.. index::
   pair: Calendar; Formats


.. _calendar_formats:

=====================
Calendar formats
=====================


.. toctree::
   :maxdepth: 2

   icalendar/icalendar
   jscalendar/jscalendar

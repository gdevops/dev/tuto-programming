.. index::
   pair: serialisation; Xavier Dupre


.. _ser_xavier_dupre:

===================================
**Tutorial de Xavier Dupre**
===================================

- http://www.xavierdupre.fr/app/teachpyx/helpsphinx/c_io/serialization.html


Definition
=============

La sérialisation est un besoin fréquent depuis l’avènement d’internet.

Une appplication web est souvent un assemblage de services indépendant
qui s’échangent des informations complexes.

Un service doit transmettre des données à un autre.

L’opération est simple lorsqu’il s’agit de transmettre un nombre réel,
un entier, une chaîne de caractère.
C’est déjà un peu plus compliqué lorsque les données à transmettre sont
constituées de listes et de dictionnaires.

Elle est complexe lorsque ce sont des instances de classes définies par
le programme lui-même.

**Or les services s’échangent uniquement des octets (ou byte en anglais).**

Il faut donc convertir des informations complexes en une séquence d’octets
puis effectuer la conversion inverse.

**Ces opérations sont regroupées sous le terme de sérialisation**.

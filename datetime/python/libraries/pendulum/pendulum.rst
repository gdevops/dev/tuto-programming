.. index::
   pair: pendulum library; Python
   ! pendulum

.. _python_pendulum_library:

=====================================
Python **pendulum library**
=====================================

- https://github.com/sdispater/pendulum
- https://pendulum.eustace.io/docs/

.. toctree::
   :maxdepth: 3

   why/why
   tips/tips
   examples/examples
   tools/tools
   versions/versions

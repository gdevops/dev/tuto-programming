.. index::
   pair:  Javascript Datetime library ; luxon
   ! luxon

.. _luxon:

========================================================================
**luxon** (A library for working with dates and times in JS)
========================================================================

.. seealso::

   - https://github.com/moment/luxon
   - https://moment.github.io/luxon/
   - https://moment.github.io/luxon/docs/manual/tour.html

.. toctree::
   :maxdepth: 3

   description/description
   duration/duration
   interval/interval
   timezones/timezones
   versions/versions

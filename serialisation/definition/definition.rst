
.. _serialisation_definition:

==========================
**Definition**
==========================

- https://rstockm.github.io/mastowall/?hashtags=python,serialisation&server=https://framapiaf.org


.. _serialisation_fr:

Définition de sérialisation en français sur wikipedia
=========================================================

- https://fr.wikipedia.org/wiki/S%C3%A9rialisation

En informatique, la **sérialisation** (de l'anglais américain serialization)
est le codage d'une information sous la forme d'une suite d'informations
plus petites (dites atomiques, voir l'étymologie de atome) pour, par
exemple, sa sauvegarde (persistance) ou **son transport sur le réseau
(proxy, RPC...)**.

L'activité réciproque, visant à décoder cette suite pour créer une copie
conforme de l'information d'origine, s'appelle la désérialisation (ou unmarshalling).

Le terme marshalling (mobilisation, canalisation, organisation) est
souvent employé de façon synonyme, de même que le terme linéarisation.

Les termes marshalling et unmarshalling s'emploient le plus souvent dans
le contexte d'échanges entre programmes informatiques, alors que les
termes sérialisation et désérialisation sont plus généraux.

D'apparence simple, ces opérations posent en réalité un certain nombre
de problèmes, comme la gestion des références entre objets ou la portabilité
des encodages.

Par ailleurs, les choix entre les diverses techniques de sérialisation
ont une influence sur les critères de performances comme la taille des
suites d'octets sérialisées ou la vitesse de leur traitement.

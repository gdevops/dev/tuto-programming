.. index::
   pair: pendulum library; Python
   ! pendulum

.. _python_pendulum_examples:

=====================================
Python **pendulum examples**
=====================================

En hommage à Jina Amini
===========================

.. toctree::
   :maxdepth: 3

   calcul-du-nombre-de-jours-depuis-la-mort-de-jina-amini


Example use of pendulum_to_date and pendulum_to_datetime, caldav
====================================================================

- :ref:`pendulum_to_datetime`
- :ref:`pendulum_to_date`


.. code-block:: python
   :linenos:

    for absence in absences_employe:
        summary = absence.str_agenda()
        description = f"SAGE {summary}"
        location = "Congés/Absences"
        if absence.is_journee_complete():
            dtstart = current_day.set(hour=0, minute=0, second=0)
            dtend = current_day.add(days=1).set(
                hour=0, minute=0, second=0
            )
            # conversion vers date
            dtstart = pendulum_to_date(dtstart)
            dtend = pendulum_to_date(dtend)
        else:
            if absence.is_demi_journee_matin():
                dtstart = current_day.set(hour=8)
                dtend = current_day.set(hour=12)
            if absence.is_demi_journee_apres_midi():
                dtstart = current_day.set(hour=13)
                dtend = current_day.set(hour=17)
            # conversion vers datetime
            dtstart = pendulum_to_datetime(dtstart)
            dtend = pendulum_to_datetime(dtend)
            logger.info(f"Absence demi-journée {dtstart=} => {dtend=}")

        try:
            # Ecriture de l'événement
            new_event = the_calendar.save_event(
                dtstart=dtstart,
                dtend=dtend,
                summary=summary,
                description=description,
                location=location,
            )
            logger.info(
                f"Successfull insertion in calendar {summary} {dtstart=} {dtend=}"
            )
        except Exception as error:
            logger.error(f"{error=}")


Example iterate the days in a month
=====================================


.. code-block:: python
   :linenos:

    import datetime
    import logging
    import os

    import caldav
    import icalendar
    import pendulum
    import rich
    from caldav.lib import error
    from caldav.objects import Principal
    from django.conf import settings
    from django.core.management.base import BaseCommand
    from django.db.models import QuerySet
    from django.utils import timezone
    from employes.models_absence import Absence
    from employes.models_employe import Employe
    from employes.models_employe import StatusEmploye
    from tools.utils_time import pendulum_to_date
    from tools.utils_time import pendulum_to_datetime

    TODAY = datetime.date.today()
    CURRENT_YEAR = TODAY.year
    CURRENT_MONTH = TODAY.month

    def update_agenda_absences(
        *, year: int = CURRENT_YEAR, month: int = CURRENT_MONTH
    ) -> None:
        employes = Employe.objects.all()
        client = caldav.DAVClient(
            url=settings.NEXTCLOUD_JUPYTER_CALDAV_URL,
            username=settings.NEXTCLOUD_JUPYTER_USERNAME,
            password=password,
        )
        dav_principal = client.principal()
        try:
            calendar_name = settings.NEXTCLOUD_JUPYTER_CALENDRIER_ABSENCES
            logger.info(f"Searching {calendar_name=} ...")
            the_calendar = dav_principal.calendar(name=calendar_name)
        except Exception as error:
            logger.error(f"{calendar_name=} {error}")

        dtstart_search = pendulum.datetime(year, month, 1, tz=settings.TIME_ZONE).start_of(
            "day"
        )
        dtend_search = dtstart_search.end_of("month").end_of("day")
        # conversion vers datetime
        dtstart_search = pendulum_to_datetime(dtstart_search)
        dtend_search = pendulum_to_datetime(dtend_search)
        liste_events = the_calendar.date_search(
            start=dtstart_search, end=dtend_search, expand=True
        )
        if len(liste_events) > 0:
            for event in liste_events:
                event.delete()

        for employe in employes:
            try:
                # on se place le 1er du mois
                first_day = pendulum.datetime(year, month, 1, tz=settings.TIME_ZONE)
                last_day = first_day.end_of("month")
                current_day = first_day.set(hour=0, minute=0, second=0)
                while (
                    current_day.day <= last_day.day and current_day.month == first_day.month
                ):
                    absences_employe = Absence.objects_conges.filter(
                        employe=employe,
                        date_absence__year=year,
                        date_absence__month=month,
                        date_absence__day=current_day.day,
                    )
                    if absences_employe.exists():
                        for absence in absences_employe:
                            summary = absence.str_agenda()
                            description = f"SAGE {summary}"
                            location = "Congés/Absences"
                            if absence.is_journee_complete():
                                dtstart = current_day.set(hour=0, minute=0, second=0)
                                dtend = current_day.add(days=1).set(
                                    hour=0, minute=0, second=0
                                )
                                # conversion vers date
                                dtstart = pendulum_to_date(dtstart)
                                dtend = pendulum_to_date(dtend)
                            else:
                                if absence.is_demi_journee_matin():
                                    dtstart = current_day.set(hour=8)
                                    dtend = current_day.set(hour=12)
                                if absence.is_demi_journee_apres_midi():
                                    dtstart = current_day.set(hour=13)
                                    dtend = current_day.set(hour=17)
                                # conversion vers datetime
                                dtstart = pendulum_to_datetime(dtstart)
                                dtend = pendulum_to_datetime(dtend)
                                logger.info(f"Absence demi-journée {dtstart=} => {dtend=}")

                            try:
                                # Ecriture de l'événement
                                new_event = the_calendar.save_event(
                                    dtstart=dtstart,
                                    dtend=dtend,
                                    summary=summary,
                                    description=description,
                                    location=location,
                                )
                                logger.info(
                                    f"Successfull insertion in calendar {summary} {dtstart=} {dtend=}"
                                )
                            except Exception as error:
                                logger.error(f"{error=}")

                    current_day = current_day.add(days=1)

            except Exception as error:
                logger.info(f"{error=}")

.. index::
   pair: iCalendar; Clients

.. _icalendar_clients:

================================================
iCalendar clients
================================================


.. toctree::
   :maxdepth: 6

   icspy/icspy
   icalendar/icalendar
   icalevents/icalevents

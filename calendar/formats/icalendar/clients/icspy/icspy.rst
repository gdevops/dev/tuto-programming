.. index::
   pair: python ; icspy
   ! icspy

.. _python_icspy:

==========================================================================
**ics.py : Pythonic and easy iCalendar library (rfc5545)**
==========================================================================

- https://github.com/ics-py/ics-py
- https://github.com/ics-py/ics-py/graphs/contributors
- https://icspy.readthedocs.io/en/stable/


.. figure:: images/logo.png
   :align: center



Description
=============

**Ics.py** is a pythonic and easy iCalendar library.

Its goals are to read and write ics data in a developer friendly way.

iCalendar is a widely-used and useful format but not user friendly.
Ics.py is there to give you the ability of creating and reading this
format without any knowledge of it.

It should be able to parse every calendar that respects the rfc5545 and
maybe some more… It also outputs rfc compliant calendars.

iCalendar (file extension .ics) is used by Google Calendar, Apple Calendar,
Android and many more.

**Ics.py is available for Python 3.7, 3.8, 3.9, 3.10, 3.11** and is Apache2 Licensed.


The iCalendar specification is complicated, you don’t like RFCs but you
want/have to use the ics format and you love pythonic APIs ? ics.py is for you !


Installation
========================================

- https://github.com/ics-py/ics-py
- https://icspy.readthedocs.io/en/stable/
- https://github.com/neogeny/TatSu


::

    poetry add ics


::

    Using version ^0.7.2 for ics

    Updating dependencies
    Resolving dependencies... (1.0s)

    Writing lock file

    Package operations: 2 installs, 0 updates, 0 removals

      • Installing tatsu (5.8.3)
      • Installing ics (0.7.2)


.. index::
   pair: f-string ; Tutorials

.. _f_string_tutorials:

==========================
f-string tutorials
==========================

.. toctree::
   :maxdepth: 3

   pep_0498/pep_0498
   miguendes/miguendes
   realpython/realpython

.. index::
   pair: pendulum; tips
   pair: pendulum; set
   pair: pendulum; start_of
   pair: pendulum; end_of


.. _pendulum_tips:

=====================================
**pendulum tips**
=====================================

- https://github.com/sdispater/pendulum
- https://pendulum.eustace.io/
- https://pendulum.eustace.io/docs/


Comment écrire le nom des mois en français ?
===============================================

::

    current_date = pendulum.datetime(2023, 1, 1, tz='Europe/Paris')
    for _ in range(1,13):
        str_month_fr = current_date.format("MMMM", locale="fr")
        print(f"{str_month_fr}")
        current_date = current_date.add(months=1)

::

    janvier
    février
    mars
    avril
    mai
    juin
    juillet
    août
    septembre
    octobre
    novembre
    décembre


from_date = pendulum.datetime(2023, 1, 1, tz='Europe/Paris')
==============================================================

::

    from_date = pendulum.datetime(2023, 1, 1, tz='Europe/Paris')


to_date = from_date.end_of("year")
=====================================

::

    to_date = from_date.end_of("year")


pendulum tips (**set method**)
================================


**set** (hour=0, minute=0, second=0)
-------------------------------------------------

::

    from_date = from_date.set(hour=0, minute=0, second=0)
    to_date = to_date.set(hour=23, minute=59, second=59)


Modifiers (end_of, start_of)
===============================

.. seealso::

   - https://pendulum.eustace.io/docs/#modifiers

**start_of** ("week")
-------------------------

::

    dt.start_of('week')

    In [11]: dt = pendulum.now()
    In [12]: start_week = dt.start_of("week")
    In [13]: start_week
    Out[13]: DateTime(2021, 1, 4, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    In [14]: start_week.day_of_week
    Out[14]: 1
    In [15]: week_prec = start_week.add(days=-7)
    In [16]: week_prec
    Out[16]: DateTime(2020, 12, 28, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    In [17]: week_prec.day_of_week
    Out[17]: 1


**end_of** ("month")
--------------------------------

::

    end_date = start_date.end_of("month")


**start_of** ("month")
-------------------------

::

    month_date = month_date.add(months=-1).start_of("month")

**start_of** ("day")
-------------------------

Example 1
+++++++++++

::

    from django.conf import settings

    ...

    today = pendulum.now(tz=settings.TIME_ZONE).start_of("day")


Example2 with Q Django selection
++++++++++=========================


.. code-block:: python
   :linenos:

    import pendulum

    # https://github.com/andialbrecht/sqlparse
    import sqlparse

    from django.conf import settings
    from django.db.models import Q
    from django.db import models, connection, reset_queries
    from tools.utils_time import id3_format_timedelta, log_sql_queries



    @staticmethod
    def get_weekly_hours_for_all_projects(employe: Employe, liste_jours: list[pendulum.datetime]) -> (str, QuerySet):
        """Retourne la somme des heures pour tous les projets d'un employé
           et une liste de jours donnée

           Retourne:

           - une chaine de caractères de la forme: (Ex:'9h30')
           - le queryset correspondant
        """
        reset_queries()
        select_datetime_begin = liste_jours[0].start_of("day")
        select_datetime_end = liste_jours[-1].start_of("day")
        liste_fiches = FicheTemps.objects.filter(
            Q(created__gte=select_datetime_begin) & Q(created__lte=select_datetime_end),
            employe__login=employe.login,
        ).exclude(projet_id__in=PROJETS_CONGES_ID)
        sql_query = sqlparse.format(str(liste_fiches.query), reindent=True)
        # logger.info(f"\nSQL query:\n{sql_query}\n")
        # calcul du total des temps
        q_total_temps_impute = liste_fiches.aggregate(Sum("temps_impute"))
        weekly_hours_for_all_projects = id3_format_timedelta(
            q_total_temps_impute["temps_impute__sum"]
        )
        # logger.info(f"{weekly_hours_for_all_projects=}")
        log_sql_queries(connection)

        return weekly_hours_for_all_projects, liste_fiches


::

    SQL query:
    SELECT SUM("fiche_temps"."temps_impute") AS "temps_impute__sum"
    FROM "fiche_temps"
    INNER JOIN "employe" ON ("fiche_temps"."id_employe" = "employe"."id")
    WHERE ("fiche_temps"."created" >= '2020-04-20T00:00:00+02:00'::timestamptz
           AND "fiche_temps"."created" <= '2020-04-28T00:00:00+02:00'::timestamptz
           AND "employe"."login" = 'pvergain'
           AND NOT ("fiche_temps"."id_projet" IN (6,
                                                  9,
                                                  14,
                                                  13,
                                                  12,
                                                  10,
                                                  15,
                                                  8,
                                                  5,
                                                  4,
                                                  3,
                                                  2,
                                                  1,
                                                  11,
                                                  7)))
    time='0.037'



convert a datetime.datetime to pendulum.datetime
===================================================

::

    import pendulum
    from django.conf import settings

    from django.utils.dateparse import (
        parse_duration,
        parse_datetime,
    )


    created = parse_datetime(created_str)
    created = pendulum.instance(created,tz=settings.TIME_ZONE)


Addition and Subtraction
=========================

Previous week
---------------

::

    In [11]: dt = pendulum.now()
    In [12]: start_week = dt.start_of("week")
    In [13]: start_week
    Out[13]: DateTime(2021, 1, 4, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    In [14]: start_week.day_of_week
    Out[14]: 1
    In [15]: week_prec = start_week.add(days=-7)
    In [16]: week_prec
    Out[16]: DateTime(2020, 12, 28, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))

Previous month
----------------


.. code-block:: python

    In [6]: dt = pendulum.now()
    In [7]: dt
    Out[7]: DateTime(2021, 1, 8, 15, 15, 37, 792012, tzinfo=Timezone('Europe/Paris'))
    In [9]: last_month = a.subtract(months=1)
    In [10]: last_month
    Out[10]: DateTime(2020, 12, 8, 15, 15, 37, 792012, tzinfo=Timezone('Europe/Paris'))

How to get monday and sunday drom the current day ?
=========================================================


::

    import pendulum
    today = pendulum.now(tz="Europe/Paris")
    from_date = today.start_of("week")
    to_date = today.end_of("week")

and for the next monday and sunday::

    from_date_next_week = from_date.add(weeks=1)
    to_date_next_week = to_date.add(weeks=1)

::

    In [10]: print(f"{today=}\n{from_date=}\n{to_date=}\n{from_date_next_week=}\n{to_date_next_week=}")

::

    today=DateTime(2023, 3, 31, 13, 26, 57, 405336, tzinfo=Timezone('Europe/Paris'))
    from_date=DateTime(2023, 3, 27, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    to_date=DateTime(2023, 4, 2, 23, 59, 59, 999999, tzinfo=Timezone('Europe/Paris'))
    from_date_next_week=DateTime(2023, 4, 3, 0, 0, 0, tzinfo=Timezone('Europe/Paris'))
    to_date_next_week=DateTime(2023, 4, 9, 23, 59, 59, 999999, tzinfo=Timezone('Europe/Paris'))

.. index::
   pair: duration ; temporal
   ! temporal duration

.. _temporal_duration:

=====================================
Javascript **temporal duration**
=====================================

.. seealso::

   - https://tc39.es/proposal-temporal/docs/duration.html
   - https://en.wikipedia.org/wiki/ISO_8601#Durations




Temporal.Duration
====================

A Temporal.Duration represents an immutable duration of time which can
be used in date/time arithmetic.

Temporal.Duration can be constructed directly or returned from
Temporal.Duration.from().

It can also be obtained from the difference() method of any other
Temporal type that supports arithmetic, and is used in those types'
plus() and minus() methods.

When printed, a Temporal.Duration produces a string according to the
ISO 8601 notation for durations.

The examples in this page use this notation extensively.

Briefly, the ISO 8601 notation consists of a P character, followed by
years, months, and days, followed by a T character, followed by hours,
minutes, and seconds with a decimal part, each with a single-letter suffix
that indicates the unit.

Any zero components may be omitted. For more detailed information,
see the ISO 8601 standard or the `Wikipedia page`_.

.. _`Wikipedia page`:  https://en.wikipedia.org/wiki/ISO_8601#Durations

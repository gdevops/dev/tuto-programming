.. index::
   pair: cookbok ; temporal
   ! temporal cookbok

.. _temporal_cookbook:

=====================================
Javascript **temporal cookbok**
=====================================

.. seealso::

   - https://tc39.es/proposal-temporal/docs/cookbook.html
   - https://github.com/tc39/proposal-temporal/tree/main/polyfill#running-cookbook-files





How to get the current date and time in the local time zone ?
==================================================================


.. code-block:: javascript
   :linenos:

    /**
     * Get the current date in JavaScript
     * This is a popular question on Stack Overflow for dates in JS
     * https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
     *
     */

    const dateTime = Temporal.now.dateTime(); // Gets the current date
    dateTime.toString(); // returns the date in ISO 8601 date format

.. index::
   pair: pendulum library; Jina Amini


.. _mort_jina_amini:

=========================================================================
Calcul du nombre de jours et semaines depuis la mort de Jina Amini
=========================================================================

- :ref:`iran_luttes:jina_amini`

.. figure:: jina_amini_kurde.png
   :align: center


.. literalinclude:: durees_jina_amini.py
   :linenos:



Le samedi 3  décembre 2022
==============================

::

    ✦ ❯ python durees_jina_amini.py --limit 2022-12-03

::

    Jina Amini est morte le vendredi 16 septembre 2022;
    ce samedi 03 décembre 2022 cela fait donc 2 mois 2 semaines 3 jours
    => 78 jours = on est dans la 12e semaine de la révolution.



Les 100 premiers jours 17 septembre au 25 décembre 2022
===========================================================

::

    python durees_jina_amini.py --nbdays 100


.. literalinclude::  100_jours.txt
   :linenos:

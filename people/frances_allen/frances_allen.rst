
.. index::
   pair: Frances ; Allen

.. _frances_allen:

=====================
Frances Allen
=====================




Par Martin Untersinger Publié le 10 août 2020 à 15h03 - Mis à jour le 10 août 2020 à 20h30
===============================================================================================

.. seealso::

   - https://www.lemonde.fr/disparitions/article/2020/08/10/frances-allen-pionniere-de-l-informatique-est-morte_6048608_3382.html


Agée de 88 ans, elle a passé toute sa carrière à IBM, où elle a donné
ses lettres de noblesse à la compilation, un des piliers de l’informatique moderne.

Seriez-vous en train de lire ces lignes sans Frances Allen ?

Il est permis d’en douter : cette informaticienne américaine a apporté,
dès les années 1960, une contribution décisive à l’informatique en donnant
ses lettres de noblesse à la compilation, qui permet de transformer un
code informatique écrit par des humains en instructions compréhensibles
par un ordinateur.

Cette pionnière de l’informatique est morte mardi 4 août, le jour de
son quatre-vingt-huitième anniversaire.

Elle avait reçu en 2006, pour l’ensemble de sa carrière, le prix Turing.

Elle fut la première femme à recevoir cette récompense, considérée comme
l’équivalent d’un prix Nobel pour l’informatique.


Une enfance sans électricité
===============================

Fille d’un paysan et d’une ancienne maîtresse d’école, elle naît et
grandit avec ses cinq frères et sœurs à Peru, une petite bourgade de
l’état de New York, non loin de la frontière canadienne, dans une
ferme dépourvue d’eau courante et d’électricité.

Après avoir enseigné les mathématiques, elle suit à l’université des
cours d’informatique, parmi les premiers dispensés dans le pays.

Elle est embauchée dans ce domaine en 1957 par IBM : elle espère y
travailler le temps de rembourser l’emprunt contracté pour ses études
avant de retourner enseigner les mathématiques.

Elle n’a finalement jamais quitté l’informatique, demeurant dans
l’entreprise jusqu’à sa retraite, en 2002.

::

    « Les compilateurs d’aujourd’hui reposent encore sur des techniques
    qu’elle a inventées », l’Association for Computing Machinery (ACM)

C’est là qu’elle a, avec son équipe, perfectionné le principe de
compilation et l’a propulsé dans de nouvelles dimensions.

Initialement, les premiers programmeurs donnaient leurs instructions
aux ordinateurs dans leur langue, faite de zéro et de un.

Rapidement, on comprit qu’il serait plus efficace de parler aux ordinateurs
en utilisant un langage plus proche de celui des humains : le code informatique.

Encore fallait-il trouver un traducteur entre ces deux langues : c’est
le rôle du compilateur.

Sans lui, nos smartphones ou nos ordinateurs seraient incapables de
comprendre ce que leur demandent de faire leurs concepteurs et leurs
utilisateurs.

Très criticable : trvailler pous la NSA pour les écoutes
===========================================================

Frances Allen a notamment travaillé sur un supercalculateur de la NSA !!!
destiné à analyser les éléments récoltés par l’agence de renseignement,
spécialisée dans les écoutes !!!, et conçu pour cette machine un compilateur
capable d’interpréter trois langages de programmation différents.

« Un objectif extraordinairement ambitieux pour l’époque » note l’Association
for Computing Machinery (ACM), qui octroie le prix Turing.

Elle n’a ensuite cessé de poursuivre ces travaux, notamment en adaptant
les compilateurs à l’arrivée des microprocesseurs.

« Les compilateurs d’aujourd’hui reposent encore sur des techniques
qu’elle a inventées », notait l’ACM lors de la remise de son prix Turing.



Témoin d’une époque révolue
==================================

Avec sa mort disparaît aussi un des derniers témoins d’une époque à ce
jour révolue dans l’histoire de l’informatique : celle où les femmes y
étaient plus nombreuses que les hommes.

Elle entre dans cette discipline dans les années 1950 : « une période
formidable pour les femmes », se remémorait-elle en 2001 dans une
interview.

Les choses se gâtent dans les années 1960 et 1970, lorsque l’informatique
devient un domaine scientifique et industriel à part entière, un processus
qui conduira à l’apparition d’un « plafond de verre », auquel elle
estime avoir été elle-même confrontée.

Il n’est ainsi guère surprenant que Frances Allen se soit engagée, durant
les deux dernières décennies de sa vie, à la promotion de la place des
femmes dans l’informatique. « Une des nombreuses choses que Frances a
réalisées, c’est attirer des femmes dans sa discipline » a salué Jeanne
Ferrante, une de ses anciennes collègues, dans le New York Times.

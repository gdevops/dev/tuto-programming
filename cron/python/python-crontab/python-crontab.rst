
.. index::
   pair: python ; crontab
   ! python-crontab

.. _python_crontab:

====================================================================================================================================================
**python-crontab: Crontab module for reading and writing crontab files and accessing the system cron automatically and simply using a direct API**
====================================================================================================================================================

- https://pypi.org/project/python-crontab/
- https://gitlab.com/doctormo/python-crontab/


.. figure:: images/logo.png
   :align: center


Description
==============

**Crontab** module for reading and writing crontab files and accessing the
system cron automatically and simply using a direct API.

.. index::
   pair: now ; temporal
   ! temporal now

.. _temporal_now:

=====================================
Javascript **temporal now**
=====================================

.. seealso::

   - https://tc39.es/proposal-temporal/docs/now.html




Temporal.now
================

The Temporal.now object has several methods which give information about
the current time and date.

::

    Temporal.now.absolute() - get the current system absolute time
    Temporal.now.timeZone() - get the current system time zone
    Temporal.now.dateTime() - get the current system date/time
    Temporal.now.time() - get the current system time
    Temporal.now.date() - get the current system date

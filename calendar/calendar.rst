.. index::
   ! calext


.. _tuto_calendar:

=====================
Calendar
=====================

- https://datatracker.ietf.org/group/calext/documents/

.. toctree::
   :maxdepth: 2

   formats/formats
   protocoles/protocoles
   python/python

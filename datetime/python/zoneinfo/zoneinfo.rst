.. index::
   pair: python ; zoneinfo

.. _python_zoneinfo:

====================================================================================================================
Python **zoneinfo module** (PEP-0615, Support for the IANA Time Zone Database in the Standard Library Python 3.9+)
====================================================================================================================

.. seealso::

   - https://www.python.org/dev/peps/pep-0615/
   - https://github.com/python/peps/blob/master/pep-0615.rst
   - https://discuss.python.org/t/pep-615-support-for-the-iana-time-zone-database-in-the-standard-library/3468
   - https://mail.python.org/archives/list/python-dev@python.org/thread/MDR2FL66R4T4VSLUI5XRFFUTKD43FMK4/
   - :ref:`python_3_9_0`





PEP-0615
=========

Proposal
-----------

This PEP has three main concerns:

- The semantics of the zoneinfo.ZoneInfo class (zoneinfo-class)
- Time zone data sources used (data-sources)
- Options for configuration of the time zone search path (search-path-config)

Because of the complexity of the proposal, rather than having separate
"specification" and "rationale" sections the design decisions and
rationales are grouped together by subject.

Example 1
------------

::

    >>> NYC0 = ZoneInfo("America/New_York")
    >>> NYC0 is ZoneInfo("America/New_York")
    True
    >>> ZoneInfo.clear_cache()
    >>> NYC1 = ZoneInfo("America/New_York")
    >>> NYC0 is NYC1
    False
    >>> NYC1 is ZoneInfo("America/New_York")
    True

Example 2
------------

::

    now = datetime.now(tz=ZoneInfo("Europe/Paris"))
    version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"

    from datetime import datetime

    # https://docs.python.org/3.9/library/zoneinfo.html
    from zoneinfo import ZoneInfo

String representation
+++++++++++++++++++++++++++

The ZoneInfo class's __str__ representation will be drawn from the key
parameter.
This is partially because the key represents a human-readable "name" of
the string, but also because it is a useful parameter that users will
want exposed.

It is necessary to provide a mechanism to expose the key for serialization
between languages and because it is also a primary key for localization
projects like CLDR_ (the Unicode Common Locale Data Repository [8]).

An example::

    >>> zone = ZoneInfo("Pacific/Kwajalein")
    >>> str(zone)
    'Pacific/Kwajalein'

    >>> dt = datetime(2020, 4, 1, 3, 15, tzinfo=zone)
    >>> f"{dt.isoformat()} [{dt.tzinfo}]"
    '2020-04-01T03:15:00+12:00 [Pacific/Kwajalein]'


.. _CLDR: http://cldr.unicode.org/#TOC-How-to-Use

Sources for time zone data
-----------------------------

One of the hardest challenges for IANA time zone support is keeping the
data up to date; between 1997 and 2020, there have been between 3 and 21
releases per year, often in response to changes in time zone rules with
little to no notice (see [10] for more details).

In order to keep up to date, and to give the system administrator
control over the data source, we propose to use system-deployed time
zone data wherever possible.

However, not all systems ship a publicly accessible time zone database,
notably Windows uses a different system for managing time zones, and so
if available zoneinfo falls back to an installable first-party package,
tzdata, available on PyPI.
If no system zoneinfo files are found but tzdata is installed, the
primary ZoneInfo constructor will use tzdata as the time zone source.

System time zone information
-------------------------------

Many Unix-like systems deploy time zone data by default, or provide a
canonical time zone data package (often called tzdata, as it is on Arch Linux,
Fedora, and Debian).
Whenever possible, it would be preferable to defer to the system time
zone information, because this allows time zone information for all
language stacks to be updated and maintained in one place.

Python distributors are encouraged to ensure that time zone data is
installed alongside Python whenever possible (e.g. by declaring tzdata
as a dependency for the python package).

The zoneinfo module will use a "search path" strategy analogous to the
PATH environment variable or the sys.path variable in Python;
the zoneinfo.TZPATH variable will be read-only (see search-path-config
for more details), ordered list of time zone data locations to search.

When creating a ZoneInfo instance from a key, the zone file will be
constructed from the first data source on the path in which the key
exists, so for example, if TZPATH were::

    TZPATH = (
        "/usr/share/zoneinfo",
        "/etc/zoneinfo"
        )

and (although this would be very unusual) /usr/share/zoneinfo contained
only America/New_York and /etc/zoneinfo contained both America/New_York
and Europe/Moscow, then ZoneInfo("America/New_York") would be satisfied
by /usr/share/zoneinfo/America/New_York, while ZoneInfo("Europe/Moscow")
would be satisfied by /etc/zoneinfo/Europe/Moscow.

At the moment, on Windows systems, the search path will default to empty,
because Windows does not officially ship a copy of the time zone database.

On non-Windows systems, the search path will default to a list of the
most commonly observed search paths.

Although this is subject to change in future versions, at launch the
default search path will be::

    TZPATH = (
        "/usr/share/zoneinfo",
        "/usr/lib/zoneinfo",
        "/usr/share/lib/zoneinfo",
        "/etc/zoneinfo",
    )

This may be configured both at compile time or at runtime; more
information on configuration options at `search-path-config`_.

.. _`search-path-config`: https://www.python.org/dev/peps/pep-0615/#search-path-config


Docker and zoneinfo
=====================

.. seealso::

   - :ref:`article_timezone_docker_2020_04`

.. index::
   pair: Structured ; Concurrency
   ! Structured Concurrency

.. _structured_concurrency:

============================
**Structured Concurrency**
============================

.. seealso::

   - https://en.wikipedia.org/wiki/Structured_concurrency
   - https://250bpm.com/blog:71/
   - https://mattwestcott.co.uk/blog/structured-concurrency-in-python-with-anyio
   - https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/
   - https://trio.discourse.group/c/structured-concurrency/7
   - https://medium.com/@elizarov/structured-concurrency-722d765aa952
   - https://wiki.openjdk.java.net/display/loom/Structured+Concurrency





Anyio
=======

.. seealso::

   - :ref:`anyio`
   - :ref:`summary_anyio`

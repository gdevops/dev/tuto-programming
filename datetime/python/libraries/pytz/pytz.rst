.. index::
   pair: pytz library; Python

.. _python_pytz_library:

================================================================
Python **pytz library** (World Timezone Definitions for Python)
================================================================


.. seealso::

   - http://pytz.sourceforge.net
   - :ref:`python_tzinfo_class`




Description
=================

**pytz** brings the Olson tz database into Python.

This library allows accurate and cross platform timezone calculations using
Python 2.4 or higher.

It also solves the issue of ambiguous times at the end of daylight saving time,
which you can read more about in the Python Library Reference (datetime.tzinfo).

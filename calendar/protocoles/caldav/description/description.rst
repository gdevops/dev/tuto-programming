
.. _description_caldav:

========================================================================
Description du protocole **CalDAV (Calendaring Extensions to WebDAV)**
========================================================================

https://fr.wikipedia.org/wiki/CalDAV
=======================================

- https://fr.wikipedia.org/wiki/CalDAV


Décrit dans la RFC 47911, **CalDAV** définit un **protocole d'édition de
calendrier en ligne**.

Son concurrent, le protocole **Web Calendar Access Protocol**, définit le
partage de fichiers au format iCalendar utilisant `WebDAV <https://en.wikipedia.org/wiki/WebDAV>`_

**CalDAV** contrairement à ce dernier, n'est pas un partage de fichiers
calendriers, mais d'évènements.

Un calendrier est, dans CalDAV, un dossier contenant des évènements,
des tâches... Chaque évènement est transmis sous la forme d'un fichier
VEVENT, VTASK.

Il est donc possible, contrairement à **Web Calendar Access Protocol** de
manipuler un seul élément sans avoir à échanger l'ensemble du calendrier.

**CalDAV** regroupe au sein d'un même usage plusieurs extensions à
WebDAV : WebDAV ACL pour les droits d'accès.

On peut noter que l'élément atomique étant l'évènement, on peut définir
des droits différents pour chaque évènement ; le protocole internet
Delta-v définissant un protocole de gestion de version sur WebDAV.

Un calendrier étant un dossier (une collection au sens WebDAV) ne
contenant que des éléments de calendrier, **CalDAV** définit un verbe
supplémentaire pour la création d'un calendrier : MKCALENDAR.



https://en.wikipedia.org/wiki/CalDAV
=========================================

- https://en.wikipedia.org/wiki/CalDAV


Calendaring Extensions to WebDAV, or **CalDAV**, is an Internet standard
allowing a client to access and manage calendar data along with the
ability to schedule meetings with users on the same or on remote servers

.. index::
   pair: Datetime ; Duration Field

.. _models_duration_field:

============================
Models Duration field
============================

.. seealso::

   - https://github.com/django/django/blob/master/django/db/models/fields/__init__.py
   - https://docs.djangoproject.com/en/dev/ref/models/fields/#durationfield
   - :ref:`postgresql_interval_field`
   - :ref:`python_timedelta`





DurationField
================

A field for storing periods of time - modeled in Python by timedelta.

When used on PostgreSQL, the data type used is an interval and on Oracle the
data type is INTERVAL DAY(9) TO SECOND(6).

Otherwise a bigint of microseconds is used.

.. note:: Arithmetic with DurationField works in most cases.
   However on all databases other than PostgreSQL, comparing the value of a
   DurationField to arithmetic on DateTimeField instances will not work as
   expected.



Example
===========

::

    class FicheTemps(models.Model):
        """La fiche de temps.

        Le temps imputé dans une fiche de temps concerne:

        - un employé
        - un projet
        - un jour

        Une fiche de temps peut être dans l'état "FICHE_EN_CALCUL' dans le cas
        où une tâche périodique met à jour le temps imputé.

        $ python manage_dev.py shell_plus

        >>> from datetime import datetime, timedelta
        >>> from fiches_temps.models import FICHE_STATUS
        >>> employe = Employe.objects.get(login='pvergain')
        >>> employe
        Out[4]: <Employe: Patrick VERGAIN>
        >>> projet = Projet.objects.get(code_chrono='PSER5N001')
        >>> projet
        Out[5]: <Projet: PSER5N001>
        >>> temps_impute=timedelta(seconds=1)
        >>> fiches_temps = FicheTemps.objects.create(
                employe=employe,
                projet=projet,
                created=timezone.now,
                temps_impute=temps_impute,
                etat=FICHE_STATUS['FICHE_EN_CALCUL']
            )
        >>> fiches_temps
        Out[19]: <FicheTemps: FDT: PSER5N001/2016-02-26 14:28:30.494177 Patrick VERGAIN 0:00:01>
        >>> liste_fiches_en_calcul_employe = FicheTemps.objects.all()\
               .filter(created=now())\
               .filter(etat=FICHE_STATUS['FICHE_EN_CALCUL'])\
               .filter(employe=employe)
        >>> les_fiches_en_calcul = FicheTemps.objects.all()\
               .filter(etat=FICHE_STATUS['FICHE_EN_CALCUL'])



        """

        class Meta:
            managed = True
            # Ancien nom de table sybase : 'TR_FICHE_TEMPS'
            db_table = "fiche_temps"
            verbose_name_plural = _("Fiches de temps")
            ordering = ["-created"]
            # https://docs.djangoproject.com/en/dev/ref/models/constraints/#uniqueconstraint
            constraints = [
                models.UniqueConstraint(
                    fields=["employe", "projet", "created"],
                    name="unique_employe_projet_created",
                )
            ]
            # https://docs.djangoproject.com/en/dev/ref/models/indexes/
            indexes = [
                models.Index(
                    fields=["employe", "projet", "created"],
                    name="employe_projet_created_index",
                )
            ]

        employe = models.ForeignKey(
            "employes.Employe",
            to_field="id",
            limit_choices_to={"etat": StatusEmploye.EMPLOYE_ID3.value},
            on_delete=models.CASCADE,
            db_column="id_employe",
            related_name="fiches_temps_employe",
            related_query_name="fiche_temps_employe",
            help_text=_("L'employé"),
            verbose_name=_("L'employé"),
        )
        projet = models.ForeignKey(
            "projets.Projet",
            to_field="id",
            db_column="id_projet",
            related_name="fiches_temps_projet",
            related_query_name="fiche_temps_projet",
            on_delete=models.CASCADE,
            help_text=_("Le projet sur lequel imputer le temps"),
            verbose_name=_("Projet"),
        )
        # ancien nom sybase: temps
        temps_impute = models.DurationField(
            default=timedelta(hours=0),
            help_text=_("Le temps impute"),
            verbose_name=_("Temps imputé"),
        )
        commentaire = models.TextField(
            default="", help_text=_("Commentaire"), verbose_name=_("Commentaire")
        )
        etat = models.PositiveSmallIntegerField(
            choices=FICHE_STATUS,
            # Valeur par défaut
            default=FICHE_STATUS.FICHE_MODIFIABLE,
            help_text=_("indique l'état de la fiche"),
            verbose_name=_("Etat fiche"),
        )
        # https://docs.djangoproject.com/en/dev/ref/models/fields/#datetimefield
        created = models.DateTimeField(
            editable=True,
            help_text=_("Date d'imputation"),
            verbose_name=_("Date imputation"),
        )
        # https://docs.djangoproject.com/en/dev/ref/models/fields/#datetimefield
        modified = models.DateTimeField(
            null=True,
            editable=True,
            # auto_now=True,
            help_text=_("Modifié le"),
            verbose_name=_("Modifié le"),
        )

        def get_absolute_url(self):
            """
            https://docs.djangoproject.com/en/dev/ref/class-based-views/generic-editing/
            """
            return reverse("fiches_temps:detail", kwargs={"pk": self.pk})

        def str_etat_fiche(self):
            return FICHE_STATUS.for_value(self.etat).display

        def __str__(self):
            message = (
                f"FDT: projet:{self.projet} created:{self.created} "
                f"modified:{self.modified} employe:{self.employe} "
                f"etat:{self.str_etat_fiche()} "
                f"temps impute:{self.temps_impute}"
                f"commentaire:{self.commentaire}"
            )
            return message



**django.utils.parse_duration**
=================================




Models DurationField
=======================

.. code-block:: python
   :linenos:


    class DurationField(Field):
        """
        Store timedelta objects.
        Use interval on PostgreSQL, INTERVAL DAY TO SECOND on Oracle, and bigint
        of microseconds on other databases.
        """
        empty_strings_allowed = False
        default_error_messages = {
            'invalid': _('“%(value)s” value has an invalid format. It must be in '
                         '[DD] [[HH:]MM:]ss[.uuuuuu] format.')
        }
        description = _("Duration")

        def get_internal_type(self):
            return "DurationField"

        def to_python(self, value):
            if value is None:
                return value
            if isinstance(value, datetime.timedelta):
                return value
            try:
                parsed = parse_duration(value)
            except ValueError:
                pass
            else:
                if parsed is not None:
                    return parsed

            raise exceptions.ValidationError(
                self.error_messages['invalid'],
                code='invalid',
                params={'value': value},
            )

        def get_db_prep_value(self, value, connection, prepared=False):
            if connection.features.has_native_duration_field:
                return value
            if value is None:
                return None
            return duration_microseconds(value)

        def get_db_converters(self, connection):
            converters = []
            if not connection.features.has_native_duration_field:
                converters.append(connection.ops.convert_durationfield_value)
            return converters + super().get_db_converters(connection)

        def value_to_string(self, obj):
            val = self.value_from_object(obj)
            return '' if val is None else duration_string(val)

        def formfield(self, **kwargs):
            return super().formfield(**{
                'form_class': forms.DurationField,
                **kwargs,
            })

**django/db/models/fields/__init__.py**
==========================================


.. literalinclude:: __init__.py
   :linenos:

.. index::
   pair: dateparse ; Django
   pair: parse_duration ; Django
   pair: parse_datetime ; Django


.. _django_dateparse:

===========================================================
**django.utils.dateparse.py**
===========================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/utils/dateparse.py
   - https://github.com/django/django/blob/master/tests/utils_tests/test_dateparse.py





::

    $ find . -name "*dateparse.py" -print

::

    ./tests/utils_tests/test_dateparse.py
    ./django/utils/dateparse.py


django.utils.dateparse.py
=============================

.. literalinclude:: dateparse.py
   :linenos:


.. _parse_duration:

django.utils.dateparse.parse_duration
========================================

.. literalinclude:: parse_duration/parse_duration.py
   :linenos:

django.utils.dateparse.parse_datetime
=========================================

.. literalinclude:: parse_datetime/parse_datetime.py
   :linenos:

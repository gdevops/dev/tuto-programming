
.. index::
   pair: fileutils ; Python
   ! fileutils

.. _python_fileutils:

===========================================================================
Python **fileutils** from Mahmoud Hashemi (https://github.com/mahmoud/)
===========================================================================

- https://github.com/mahmoud/boltons
- https://boltons.readthedocs.io/en/latest/fileutils.html
- https://github.com/mahmoud/boltons/commits/master


Description
============

Virtually every Python programmer has used Python for wrangling disk
contents, and fileutils collects solutions to some of the most commonly-found
gaps in the standard library.

.. index::
   pair: Postgresql ; datetime queries

.. _howto_query_datetimes:

===========================================
How to Query Date and Time in PostgreSQL
===========================================

.. seealso::

   - https://popsql.com/learn-sql/postgresql/how-to-query-date-and-time-in-postgresql/



Get the date and time time right now
=======================================

::

    select now(); -- date and time
    select current_date; -- date
    select current_time; -- time


Find rows between two absolute timestamps
============================================

::

    select count(1)
    from events
    where time between '2020-04-01' and '2020-04-31';

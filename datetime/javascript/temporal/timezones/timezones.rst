.. index::
   pair: Javascript ; temporal timezone
   ! temporal timezone

.. _temporal_timezone:

=====================================
Javascript **temporal timezone**
=====================================

.. seealso::

   - https://tc39.es/proposal-temporal/docs/timezone.html
   - https://tc39.es/proposal-temporal/docs/timezone-draft.html



Temporal.TimeZone
=================

A Temporal.TimeZone is a representation of a time zone: either an IANA
time zone, including information about the time zone such as the offset
between the local time and UTC at a particular time, and daylight
saving time (DST) changes; or simply a particular UTC offset with no DST.

Since Temporal.Absolute and Temporal.DateTime do not contain any time
zone information, a Temporal.TimeZone object is required to convert
between the two.

Finally, the Temporal.TimeZone object itself provides access to a list
of the time zones in the IANA time zone database.

An API for creating custom time zones is under discussion.
See `Custom Time Zone Draft`_ for more information.


.. _`Custom Time Zone Draft`:  https://tc39.es/proposal-temporal/docs/timezone-draft.html


.. _news_2019_04_12_katie_bouman:

=========================
2019-04-12 Katie Bouman
=========================


.. seealso::

   - https://www.lemonde.fr/sciences/article/2019/04/12/katie-bouman-la-traqueuse-de-trou-noir-propulsee-malgre-elle-superstar-des-femmes-de-science_5449542_1650684.html?utm_medium=Social&utm_source=Twitter#Echobox=1555083156


C’est que le parcours de Katie Bouman a de quoi faire rêver.

Diplômée d’un master en génie informatique en 2011 à l’université du Michigan,
la scientifique poursuit son cursus universitaire d’excellence par un doctorat
au sein du prestigieux Massachusetts Institute of Technology (MIT).

Sa thèse est consacrée à « l’utilisation de méthodes informatiques émergentes
pour dépasser les limites de la visualisation interdisciplinaire », décrit
l’intéressée sur son riche curriculum vitae en ligne.

Dans un article publié par Time, la jeune femme décrit ainsi son travail comme
l’art de « trouver des façons de voir ou de mesurer ce qui est invisible ».


Au début des années 1980, l’historienne des sciences Margaret Rossiter avait
théorisé « l’effet Matilda », selon lequel les femmes scientifiques profitent
moins des retombées de leurs recherches, et ce souvent au profit des hommes.

L’un des cas les plus emblématiques est celui de Jocelyn Bell, qui découvrit
le premier pulsar (une étoile qui émet des signaux très régulièrement),
découverte pour laquelle son directeur de thèse, Antony Hewish, obtint le
prix Nobel en 1974.

Plus récemment, la Canadienne Donna Strickland a obtenu, en octobre 2018, avec
deux de ses collègues masculins, le prix Nobel de physique.
Mais cette spécialiste des lasers, contrairement à ses deux collaborateurs,
n’avait pas de page Wikipédia à son nom – le brouillon de page avait été
rejeté par un éditeur de l’encyclopédie en ligne faute de notoriété.

Assiste-t-on désormais à une inversion de cet « effet Matilda » ? Harcelée par
les médias du monde entier au point de devoir couper son téléphone, selon le
New York Times, Katie Bouman a exprimé sa gêne à l’idée d’être autant mise en
avant.
« Ce n’est pas un algorithme ou une personne qui a créé cette image »,
a tempéré, sur Facebook à nouveau, la chercheuse, qui s’apprête à devenir
professeure dans l’autre université scientifique majeure des Etats-Unis,
la California Institute of Technology (CalTech).

«L’accent devrait être mis sur le travail incroyable de toute l’équipe et
non pas d’une seule personne.
Se concentrer sur une seule personne comme cela n’aide personne, à
commencer par moi. »

Interrogée sur l’absence de parité dans le milieu scientifique par le Time,
Katie Bouman explique ainsi qu’il ne s’agit pas pour elle d’un sujet
d’inquiétude majeur.

« Mais j’y pense, parfois. Comment peut-on faire en sorte que plus de femmes
s’impliquent dans la science ?

Une des solutions, c’est de montrer qu’intégrer des milieux comme ceux de
l’informatique ou l’ingénierie, cela ne consiste pas seulement à s’asseoir
dans un labo pour assembler des circuits ou taper sur un clavier»,
assure la jeune femme.

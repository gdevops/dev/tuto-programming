.. index::
   pair: Datetime ; Template formatting

.. _django_datetime_template_formatting:

=============================================
Template timezone formatting (**timezone**)
=============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/templates/builtins/#date




tz (time-zones-in-templates)
==============================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/templates/builtins/#tz
   - https://docs.djangoproject.com/en/dev/topics/i18n/timezones/#time-zones-in-templates


::

    {% load tz %}

    {% timezone "Europe/Paris" %}
        Paris time: {{ value }}
    {% endtimezone %}

    {% timezone None %}
        Server time: {{ value }}
    {% endtimezone %}


Example
--------

::

    {% load tz %}
    <td> {{ log.date_transaction|timezone:"America/Bogota"|date:"d/m/Y - H:i:s" }} </td>

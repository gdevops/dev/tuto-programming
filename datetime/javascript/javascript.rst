.. index::
   pair: date ; Javascript Programming

.. _javascript_date_programming:

=====================================
**Javascript datetime management**
=====================================

.. toctree::
   :maxdepth: 3

   temporal/temporal
   libraries/libraries
   tips/tips

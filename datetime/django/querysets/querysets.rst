.. index::
   pair: querysets ; Django

.. _django_querysets_date_1:

============================
Django querysets
============================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/models/querysets
   - https://github.com/django/django/blob/master/django/db/models/functions/datetime.py
   - https://docs.djangoproject.com/en/dev/ref/models/fields/#datetimefield

.. toctree::
   :maxdepth: 3

   datetimes/datetimes
   date/date

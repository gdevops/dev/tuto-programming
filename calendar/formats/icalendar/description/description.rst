

.. _icalendar_description:

================================================
iCal/iCalendar description
================================================


https://fr.wikipedia.org/wiki/ICalendar
===========================================

- https://fr.wikipedia.org/wiki/ICalendar


iCal/iCalendar est un format de données discuté et proposé par la RFC 55451
(et ses révisions ultérieures) pour les échanges de données de calendrier


Il est notamment utilisé pour le partage de calendrier par abonnement
via les protocoles CalDAV et Web Calendar Access Protocol.

iCalendar permet aux utilisateurs d'envoyer des demandes de rendez-vous,
et de les transmettre à d'autres utilisateurs par courriel.

Les destinataires du courriel qui possèdent un logiciel supportant le
format iCalendar peuvent facilement répondre à l'expéditeur ou proposer
une autre date/heure de rendez-vous.

Les données iCalendar sont typiquement échangées en utilisant le traditionnel
courriel, mais le standard proposé a été conçu pour être indépendant du
protocole de transport.

Par exemple, il peut aussi bien être partagé et édité en utilisant un
serveur WebDAV.

Les serveurs web simplistes (ne proposant que le protocole HTTP) sont
souvent utilisés pour distribuer des données iCalendar concernant un
événement et pour publier les temps de travail d'un individu.

Les sites d'événements sur le web embarquent souvent des données iCalendar
dans leurs pages web en utilisant le protocole `hCalendar <https://fr.wikipedia.org/wiki/HCalendar>`_, une représentation
1:1 d'iCalendar, mais écrit en XHTML.


https://en.wikipedia.org/wiki/ICalendar
===========================================

- https://en.wikipedia.org/wiki/ICalendar

iCalendar was first created in 1998 by the Calendaring and Scheduling
Working Group of the Internet Engineering Task Force, chaired by
Anik Ganguly of Open Text Corporation, and was authored by Frank Dawson
of Lotus Development Corporation and Derik Stenerson of Microsoft Corporation.

iCalendar is heavily based on the earlier vCalendar by the Internet Mail
Consortium (IMC).

iCalendar data files are plain text files with the extension .ics or .ifb
(for files containing availability information only).

RFC 5545 replaced RFC 2445 in September 2009 and now defines the standard.

The filename extension of ics is to be used for files containing calendaring
and scheduling information, ifb for files with free or busy time information
consistent with this MIME content type.

The equivalent file type codes in Apple Macintosh operating system environments
are iCal and iFBf.

Limitations and future
---------------------------

The iCalendar format is designed to transmit calendar-based data, such
as events, and intentionally does not describe what to do with that data.

Thus, other programming may be needed to negotiate what to do with this data.[nb 2]

iCalendar is meant to "provide the definition of a common format for
openly exchanging calendaring and scheduling information across the Internet".

While the features most often used by users are widely supported by
iCalendar, some more advanced capabilities have problems.
For example, most vendors do not support Journals (VJOURNAL). VTODOs
have had conversion problems as well.[nb 3]

iCalendar's calendar is also not compatible with some non-Gregorian
calendars such as the lunar calendars used in Israel and Saudi Arabia.[nb 4]

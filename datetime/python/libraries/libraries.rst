.. index::
   pair: datetime libraries; Python
   ! datetime python libraries

.. _python_date_libraries:
.. _python_datetime_libraries:

=====================================
**Python datetime libraries**
=====================================

.. toctree::
   :maxdepth: 3


   isoweek/isoweek
   jours_feries_france/jours_feries_france
   pendulum/pendulum
   pytz/pytz
   workalendar/workalendar

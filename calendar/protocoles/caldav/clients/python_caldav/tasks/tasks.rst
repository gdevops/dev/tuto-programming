.. index::
   pair; Tasks; CalDAV

.. _python_caldav_tasks:

==================================================================
**Tasks**
==================================================================

- https://caldav.readthedocs.io/en/latest/index.html
- https://github.com/python-caldav
- https://github.com/python-caldav/caldav/graphs/contributors
- https://github.com/tobixen/calendar-cli
- https://github.com/python-caldav/caldav/blob/master/examples/scheduling_examples.py
- https://github.com/python-caldav/caldav/blob/master/tests/test_caldav.py


Create a task list
====================

Create a task list

.. code-block:: python

    my_new_tasklist = dav_principal.make_calendar(
                name="Test tasklist", supported_calendar_component_set=['VTODO'])

Adding a task to a task list
==============================

Adding a task to a task list. The ics parameter may be some complete
ical text string or a fragment.

.. code-block:: python

    my_new_tasklist.save_todo(
        ics = "RRULE:FREQ=YEARLY",
        summary="Deliver some data to the Tax authorities",
        dtstart=date(2020, 4, 1),
        due=date(2020,5,1),
        categories=['family', 'finance'],
        status='NEEDS-ACTION')

Fetching tasks
==================

Fetching tasks


.. code-block:: python

   todos = my_new_tasklist.todos()

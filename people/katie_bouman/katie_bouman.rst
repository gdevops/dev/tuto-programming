
.. index::
   pair: Katie ; Bouman

.. _katie_bouman:

=====================
Katie Bouman
=====================


.. seealso::

   - https://fr.wikipedia.org/wiki/Katie_Bouman
   - https://people.csail.mit.edu/klbouman/
   - https://eventhorizontelescope.org/organization
   - https://x.com/NatureNews/status/1116370136800296965

.. figure:: ../../_static/katie_bouman.jpeg
   :width: 150
   :align: center

   https://en.wikipedia.org/wiki/Katie_Bouman





French wikipedia
==================

.. seealso::

   - https://fr.wikipedia.org/wiki/Katie_Bouman
   - https://fr.wikipedia.org/wiki/Event_Horizon_Telescope

Katherine Louise Bouman, dite Katie Bouman, (née le 9 mai 1989 à West Lafayette
en Indiana) est une scientifique américaine, enseignante-chercheuse au California
Institute of Technology.

Spécialiste en traitement de l'image, elle est connue pour sa contribution
à la production de la première visualisation d'un trou noir à partir des
données de l'Event Horizon Telescope.


Recherche et carrière
-----------------------

À partir de 2016, Katie Bouman est responsable au MIT d'un algorithme de
reconstitution de l'image baptisé CHIRP (Continuous High-Résolution Image
Reconstruction using Patch priori), traitant quatre petaoctets de données
collectés par huit radiotélescopes dans le monde.

Lors d'une conférence TED en 2016, elle prédit des résultats prometteurs bien
qu’elle a « commencé ce projet sans connaissance en astrophysique » et la
difficulté de restituer une image « réaliste » sans avoir de modèle de référence.

Bouman rejoint CalTech comme professeure assistante en 2019, où elle travaille
à la recherche en imagerie computationnelle.

Bouman participe à l'élaboration de la première image d'un trou noir, présentée
publiquement en avril 2019.

Ce résultat est issu de la théorie qui prédisait que les trous noirs pouvaient
être détectés comme des « ombres » apparaissant sur l'arrière-plan formé par
les gaz chauds en rotation rapide autour d'eux.



Histoire
==========

Diplômée d’un master en génie informatique en 2011 à l’université du Michigan,
la scientifique poursuit son cursus universitaire d’excellence par un doctorat
au sein du prestigieux Massachusetts Institute of Technology (MIT).

Sa thèse est consacrée à « l’utilisation de méthodes informatiques émergentes
pour dépasser les limites de la visualisation interdisciplinaire », décrit
l’intéressée sur son riche curriculum vitae en ligne.

Dans un article publié par Time, la jeune femme décrit ainsi son travail comme
l’art de « trouver des façons de voir ou de mesurer ce qui est invisible ».


Au début des années 1980, l’historienne des sciences Margaret Rossiter avait
théorisé « l’effet Matilda », selon lequel les femmes scientifiques profitent
moins des retombées de leurs recherches, et ce souvent au profit des hommes.

L’un des cas les plus emblématiques est celui de Jocelyn Bell, qui découvrit
le premier pulsar (une étoile qui émet des signaux très régulièrement),
découverte pour laquelle son directeur de thèse, Antony Hewish, obtint le
prix Nobel en 1974.

Plus récemment, la Canadienne Donna Strickland a obtenu, en octobre 2018, avec
deux de ses collègues masculins, le prix Nobel de physique.
Mais cette spécialiste des lasers, contrairement à ses deux collaborateurs,
n’avait pas de page Wikipédia à son nom – le brouillon de page avait été
rejeté par un éditeur de l’encyclopédie en ligne faute de notoriété.

Assiste-t-on désormais à une inversion de cet « effet Matilda » ? Harcelée par
les médias du monde entier au point de devoir couper son téléphone, selon le
New York Times, Katie Bouman a exprimé sa gêne à l’idée d’être autant mise en
avant.
« Ce n’est pas un algorithme ou une personne qui a créé cette image »,
a tempéré, sur Facebook à nouveau, la chercheuse, qui s’apprête à devenir
professeure dans l’autre université scientifique majeure des Etats-Unis,
la California Institute of Technology (CalTech).

«L’accent devrait être mis sur le travail incroyable de toute l’équipe et
non pas d’une seule personne.
Se concentrer sur une seule personne comme cela n’aide personne, à
commencer par moi. »

Interrogée sur l’absence de parité dans le milieu scientifique par le Time,
Katie Bouman explique ainsi qu’il ne s’agit pas pour elle d’un sujet
d’inquiétude majeur.

« Mais j’y pense, parfois. Comment peut-on faire en sorte que plus de femmes
s’impliquent dans la science ?

Une des solutions, c’est de montrer qu’intégrer des milieux comme ceux de
l’informatique ou l’ingénierie, cela ne consiste pas seulement à s’asseoir
dans un labo pour assembler des circuits ou taper sur un clavier»,
assure la jeune femme.

Déclarations
=============

«Notre succès est dû à notre expertise interdisciplinaire. On est un mélange
d’astronomes, de physiciens, de mathématiciens et d’ingénieurs.

C’est comme ça qu’on va rendre possible l’impossible », expliquait Katie Bouman
dans sa conférence de 2017.

« J’aimerais encourager tout le monde à se lancer dans l’impossible, à pousser
les limites de la science. Même lorsque parfois, celles-ci semblent aussi
mystérieuses qu’un trou noir », conclut-elle, en souriant.


Historique
===========


.. toctree::
   :maxdepth: 6

   2019/2019

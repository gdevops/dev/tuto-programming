.. index::
   pair: datetime class; now method

.. _python_datetime_class_now_method:

============================================
Python **datetime class** now() method()
============================================




classmethod datetime.now(tz=None)
====================================

    Return the current local date and time.

    If optional argument tz is None or not specified, this is like today(), but, if possible, supplies more precision than can be gotten from going through a time.time() timestamp (for example, this may be possible on platforms supplying the C gettimeofday() function).

    If tz is not None, it must be an instance of a tzinfo subclass, and the current date and time are converted to tz’s time zone.

    This function is preferred over today() and utcnow().

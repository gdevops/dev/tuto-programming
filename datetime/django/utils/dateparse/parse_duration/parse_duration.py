"""Functions to parse datetime objects."""

# We're using regular expressions rather than time.strptime because:
# - They provide both validation and parsing.
# - They're more flexible for datetimes.
# - The date/datetime/time constructors produce friendlier error messages.

import datetime

from django.utils.regex_helper import _lazy_re_compile
from django.utils.timezone import get_fixed_timezone, utc

date_re = _lazy_re_compile(r"(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})$")

time_re = _lazy_re_compile(
    r"(?P<hour>\d{1,2}):(?P<minute>\d{1,2})"
    r"(?::(?P<second>\d{1,2})(?:[\.,](?P<microsecond>\d{1,6})\d{0,6})?)?"
)

datetime_re = _lazy_re_compile(
    r"(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})"
    r"[T ](?P<hour>\d{1,2}):(?P<minute>\d{1,2})"
    r"(?::(?P<second>\d{1,2})(?:[\.,](?P<microsecond>\d{1,6})\d{0,6})?)?"
    r"(?P<tzinfo>Z|[+-]\d{2}(?::?\d{2})?)?$"
)

standard_duration_re = _lazy_re_compile(
    r"^"
    r"(?:(?P<days>-?\d+) (days?, )?)?"
    r"(?P<sign>-?)"
    r"((?:(?P<hours>\d+):)(?=\d+:\d+))?"
    r"(?:(?P<minutes>\d+):)?"
    r"(?P<seconds>\d+)"
    r"(?:[\.,](?P<microseconds>\d{1,6})\d{0,6})?"
    r"$"
)

# Support the sections of ISO 8601 date representation that are accepted by
# timedelta
iso8601_duration_re = _lazy_re_compile(
    r"^(?P<sign>[-+]?)"
    r"P"
    r"(?:(?P<days>\d+(.\d+)?)D)?"
    r"(?:T"
    r"(?:(?P<hours>\d+(.\d+)?)H)?"
    r"(?:(?P<minutes>\d+(.\d+)?)M)?"
    r"(?:(?P<seconds>\d+(.\d+)?)S)?"
    r")?"
    r"$"
)

# Support PostgreSQL's day-time interval format, e.g. "3 days 04:05:06". The
# year-month and mixed intervals cannot be converted to a timedelta and thus
# aren't accepted.
postgres_interval_re = _lazy_re_compile(
    r"^"
    r"(?:(?P<days>-?\d+) (days? ?))?"
    r"(?:(?P<sign>[-+])?"
    r"(?P<hours>\d+):"
    r"(?P<minutes>\d\d):"
    r"(?P<seconds>\d\d)"
    r"(?:\.(?P<microseconds>\d{1,6}))?"
    r")?$"
)


def parse_duration(value):
    """Parse a duration string and return a datetime.timedelta.

    The preferred format for durations in Django is '%d %H:%M:%S.%f'.

    Also supports ISO 8601 representation and PostgreSQL's day-time interval
    format.
    """
    match = (
        standard_duration_re.match(value)
        or iso8601_duration_re.match(value)
        or postgres_interval_re.match(value)
    )
    if match:
        kw = match.groupdict()
        sign = -1 if kw.pop("sign", "+") == "-" else 1
        if kw.get("microseconds"):
            kw["microseconds"] = kw["microseconds"].ljust(6, "0")
        if (
            kw.get("seconds")
            and kw.get("microseconds")
            and kw["seconds"].startswith("-")
        ):
            kw["microseconds"] = "-" + kw["microseconds"]
        kw = {k: float(v.replace(",", ".")) for k, v in kw.items() if v is not None}
        days = datetime.timedelta(kw.pop("days", 0.0) or 0.0)
        return days + sign * datetime.timedelta(**kw)


class DurationParseTests(unittest.TestCase):
    def test_parse_python_format(self):
        timedeltas = [
            timedelta(
                days=4, minutes=15, seconds=30, milliseconds=100
            ),  # fractions of seconds
            timedelta(hours=10, minutes=15, seconds=30),  # hours, minutes, seconds
            timedelta(days=4, minutes=15, seconds=30),  # multiple days
            timedelta(days=1, minutes=00, seconds=00),  # single day
            timedelta(days=-4, minutes=15, seconds=30),  # negative durations
            timedelta(minutes=15, seconds=30),  # minute & seconds
            timedelta(seconds=30),  # seconds
        ]
        for delta in timedeltas:
            with self.subTest(delta=delta):
                self.assertEqual(parse_duration(format(delta)), delta)

    def test_parse_postgresql_format(self):
        test_values = (
            ("1 day", timedelta(1)),
            ("1 day 0:00:01", timedelta(days=1, seconds=1)),
            ("1 day -0:00:01", timedelta(days=1, seconds=-1)),
            ("-1 day -0:00:01", timedelta(days=-1, seconds=-1)),
            ("-1 day +0:00:01", timedelta(days=-1, seconds=1)),
            (
                "4 days 0:15:30.1",
                timedelta(days=4, minutes=15, seconds=30, milliseconds=100),
            ),
            (
                "4 days 0:15:30.0001",
                timedelta(days=4, minutes=15, seconds=30, microseconds=100),
            ),
            ("-4 days -15:00:30", timedelta(days=-4, hours=-15, seconds=-30)),
        )
        for source, expected in test_values:
            with self.subTest(source=source):
                self.assertEqual(parse_duration(source), expected)

    def test_seconds(self):
        self.assertEqual(parse_duration("30"), timedelta(seconds=30))

    def test_minutes_seconds(self):
        self.assertEqual(parse_duration("15:30"), timedelta(minutes=15, seconds=30))
        self.assertEqual(parse_duration("5:30"), timedelta(minutes=5, seconds=30))

    def test_hours_minutes_seconds(self):
        self.assertEqual(
            parse_duration("10:15:30"), timedelta(hours=10, minutes=15, seconds=30)
        )
        self.assertEqual(
            parse_duration("1:15:30"), timedelta(hours=1, minutes=15, seconds=30)
        )
        self.assertEqual(
            parse_duration("100:200:300"),
            timedelta(hours=100, minutes=200, seconds=300),
        )

    def test_days(self):
        self.assertEqual(
            parse_duration("4 15:30"), timedelta(days=4, minutes=15, seconds=30)
        )
        self.assertEqual(
            parse_duration("4 10:15:30"),
            timedelta(days=4, hours=10, minutes=15, seconds=30),
        )

    def test_fractions_of_seconds(self):
        test_values = (
            ("15:30.1", timedelta(minutes=15, seconds=30, milliseconds=100)),
            ("15:30.01", timedelta(minutes=15, seconds=30, milliseconds=10)),
            ("15:30.001", timedelta(minutes=15, seconds=30, milliseconds=1)),
            ("15:30.0001", timedelta(minutes=15, seconds=30, microseconds=100)),
            ("15:30.00001", timedelta(minutes=15, seconds=30, microseconds=10)),
            ("15:30.000001", timedelta(minutes=15, seconds=30, microseconds=1)),
            ("15:30,000001", timedelta(minutes=15, seconds=30, microseconds=1)),
        )
        for source, expected in test_values:
            with self.subTest(source=source):
                self.assertEqual(parse_duration(source), expected)

    def test_negative(self):
        test_values = (
            ("-4 15:30", timedelta(days=-4, minutes=15, seconds=30)),
            ("-172800", timedelta(days=-2)),
            ("-15:30", timedelta(minutes=-15, seconds=-30)),
            ("-1:15:30", timedelta(hours=-1, minutes=-15, seconds=-30)),
            ("-30.1", timedelta(seconds=-30, milliseconds=-100)),
            ("-30,1", timedelta(seconds=-30, milliseconds=-100)),
            ("-00:01:01", timedelta(minutes=-1, seconds=-1)),
            ("-01:01", timedelta(seconds=-61)),
            ("-01:-01", None),
        )
        for source, expected in test_values:
            with self.subTest(source=source):
                self.assertEqual(parse_duration(source), expected)

    def test_iso_8601(self):
        test_values = (
            ("P4Y", None),
            ("P4M", None),
            ("P4W", None),
            ("P4D", timedelta(days=4)),
            ("P0.5D", timedelta(hours=12)),
            ("P0,5D", timedelta(hours=12)),
            ("PT5H", timedelta(hours=5)),
            ("PT5M", timedelta(minutes=5)),
            ("PT5S", timedelta(seconds=5)),
            ("PT0.000005S", timedelta(microseconds=5)),
            ("PT0,000005S", timedelta(microseconds=5)),
        )
        for source, expected in test_values:
            with self.subTest(source=source):
                self.assertEqual(parse_duration(source), expected)

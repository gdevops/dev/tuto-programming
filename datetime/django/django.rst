.. index::
   pair: datetime ; Django
   pair: date ; Django

.. _django_datetime:

===========================================
Django Datetime and timezone programming
===========================================

.. seealso::

   - https://github.com/django/django/blob/master/django/db/models/functions/datetime.py
   - https://docs.djangoproject.com/en/dev/ref/models/fields/#datetimefield

.. toctree::
   :maxdepth: 3

   articles/articles
   settings/settings
   timezone/timezone
   models/models
   querysets/querysets
   forms/forms
   formatting/formatting
   utils/utils

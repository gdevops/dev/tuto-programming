
.. index::
   pair: datetime ; Timezones
   ! Timezones

.. _timezones:

==========================
Timezones
==========================

.. seealso::

   - https://github.com/eggert/tz
   - http://boussejra.com/2018/07/04/timezones-are-hell.html
   - https://data.iana.org/time-zones/tz-link.html#changes

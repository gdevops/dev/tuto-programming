
.. index::
   pair: datetime ; settings

.. _django_datetime_settings:

=================================================================
Django Datetime settings (**from django.conf import settings**)
=================================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/conf/global_settings.py
   - https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
   - https://docs.djangoproject.com/fr/2.2/ref/settings/#std:setting-TIME_ZONE
   - https://docs.djangoproject.com/en/dev/topics/i18n/timezones/





from django.conf import settings
==================================

::

    USE_TZ = True
    TIME_ZONE = "Europe/Paris"


DATETIME_INPUT_FORMATS
========================

.. seealso::

   - https://github.com/django/django/blob/master/django/conf/global_settings.py

.. code-block:: python

    # Default formats to be used when parsing dates and times from input boxes,
    # in order
    # See all available format string here:
    # https://docs.python.org/library/datetime.html#strftime-behavior
    # * Note that these format strings are different from the ones to display dates
    DATETIME_INPUT_FORMATS = [
        '%Y-%m-%d %H:%M:%S',     # '2006-10-25 14:30:59'
        '%Y-%m-%d %H:%M:%S.%f',  # '2006-10-25 14:30:59.000200'
        '%Y-%m-%d %H:%M',        # '2006-10-25 14:30'
        '%Y-%m-%d',              # '2006-10-25'
        '%m/%d/%Y %H:%M:%S',     # '10/25/2006 14:30:59'
        '%m/%d/%Y %H:%M:%S.%f',  # '10/25/2006 14:30:59.000200'
        '%m/%d/%Y %H:%M',        # '10/25/2006 14:30'
        '%m/%d/%Y',              # '10/25/2006'
        '%m/%d/%y %H:%M:%S',     # '10/25/06 14:30:59'
        '%m/%d/%y %H:%M:%S.%f',  # '10/25/06 14:30:59.000200'
        '%m/%d/%y %H:%M',        # '10/25/06 14:30'
        '%m/%d/%y',              # '10/25/06'
    ]


TIME_ZONE
============

.. seealso::

   - https://docs.djangoproject.com/fr/2.2/ref/settings/#std:setting-TIME_ZONE


Valeur par défaut : 'America/Chicago'

Une chaîne représentant le fuseau horaire pour cette installation.
Voir la `liste des fuseaux horaires`_.


.. _`liste des fuseaux horaires`: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones

Note
======

Comme Django a été initialement publié avec le réglage TIME_ZONE contenant
'America/Chicago', le réglage global (utilisé si aucun n’est défini dans le
settings.py du projet) reste 'America/Chicago' pour garantir la
rétrocompatibilité.

Les nouveaux gabarits de projet par défaut utilisent 'UTC'.

Notez que cela ne correspond pas forcément au fuseau horaire du serveur.

Par exemple, il est imaginable qu’un site serve plusieurs instances de sites
Django, chacun ayant leur propre réglage de fuseau horaire.

Lorsque USE_TZ est défini à False, ce réglage définit le fuseau horaire avec
lequel Django stocke tous les objets dates/heures.

Lorsque USE_TZ est défini à True, il s’agit alors du fuseau horaire que Django
utilise par défaut pour afficher les dates/heures dans les gabarits et pour
interpréter les valeurs dates/heures saisies dans les formulaires.

Dans les environnements Unix (où time.tzset() est implémenté), Django définit
la variable os.environ['TZ'] au fuseau horaire défini dans le réglage TIME_ZONE.
Ainsi, toutes les vues et tous les modèles opèrent automatiquement dans ce
fuseau horaire. Cependant, Django ne définit pas la variable d’environnement TZ
si vous utilisez l’option de configuration manuelle décrite dans configuration
manuelle des réglages.

Si Django ne définit pas la variable d’environnement TZ, c’est à vous de vous
assurer que les processus fonctionnent dans le bon environnement.

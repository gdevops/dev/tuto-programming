.. index::
   pair: python ; datetime

.. _python_datetime:

=====================================
Python **datetime module**
=====================================

- https://docs.python.org/dev/library/datetime.html
- https://github.com/python/cpython/blob/master/Modules/_datetimemodule.c
- https://github.com/python/cpython/blob/master/Doc/library/datetime.rst
- https://raw.githubusercontent.com/python/cpython/master/Doc/library/datetime.rst
- https://www.docstring.fr/blog/la-gestion-des-dates-avec-python/




Description
=============

The datetime module supplies classes for manipulating dates and times.

While date and time arithmetic is supported, the focus of the implementation
is on efficient attribute extraction for output formatting and manipulation.


datetime classes
==================

.. toctree::
   :maxdepth: 3

   classes/classes

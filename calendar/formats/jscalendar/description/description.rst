
.. _jscalendar_description:

================================================
JSCalendar description
================================================


- https://www.bortzmeyer.org/8984.html
- https://datatracker.ietf.org/doc/rfc8984/


.. toctree::
   :maxdepth: 3

   bortzmeyer/bortzmeyer

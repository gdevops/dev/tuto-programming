.. index::
   pair: Postgresql ; DDL fields

.. _postgresql_ddl_fields:

=====================================
Postgresql DDL fields
=====================================

.. seealso::

   - http://www.postgresqltutorial.com/postgresql-date/
   - http://www.postgresqltutorial.com/postgresql-time/
   - http://www.postgresqltutorial.com/postgresql-timestamp/
   - http://www.postgresqltutorial.com/postgresql-interval/

.. toctree::
   :maxdepth: 3

   interval/interval
   timestamptz/timestamptz

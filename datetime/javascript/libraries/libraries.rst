.. index::
   pair: Javascript ; Datetime libraries
   !  Javascript Datetime libraries

.. _javascript_libraries:

=====================================
Javascript datetime libraries
=====================================

.. seealso::

   - https://github.com/tc39/proposal-temporal/blob/main/README.md

.. toctree::
   :maxdepth: 3

   luxon/luxon

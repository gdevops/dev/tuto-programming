.. index::
   pair: Postgresql ; Timezones

.. _prog_postgresql_timezones:

===========================================
PostgreSQL **timezones**
===========================================

.. seealso::

   - :ref:`postgresql_timezones`
   - :ref:`prog_mariadb_timezones`

.. index::
   pair: python ; wadu
   ! wadu

.. _wadu:

==========================================================================
**wadu implements recurrence rules for calendar events**
==========================================================================

- https://christophercrouzet.com/blog/dev/wadu
- https://github.com/christophercrouzet/wadu
- https://x.com/christophercrzt
- https://github.com/christophercrouzet

Features
=============

Wadu implements recurrence rules for calendar events.

- sane: the implementation is fairly linear and straightforward, making
  it easy enough to follow the code paths and make sense of them.
- compliant: implements the RFC 5545 specification but also with the
  unambiguous JSCalendar draft from IETF.

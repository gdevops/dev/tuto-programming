
.. _python_caldav_calendrier:

==================================================================
**Calendrier**
==================================================================

- https://caldav.readthedocs.io/en/latest/index.html
- https://github.com/python-caldav
- https://github.com/python-caldav/caldav/graphs/contributors
- https://github.com/tobixen/calendar-cli
- https://github.com/python-caldav/caldav/blob/master/examples/scheduling_examples.py
- https://github.com/python-caldav/caldav/blob/master/tests/test_caldav.py


Description
============

This project is a **CalDAV (RFC4791) client** library for Python.

Features:

- :ref:`create <creating_a_calendar>`, modify calendar
- :ref:`find_a_calendar`
- :ref:`delete_a_calendar`
- :ref:`fetching calendars <fetching_calendars>`
- :ref:`create an event<adding_an_event_parameters>`,
- update event
- :ref:`delete event <delete_an_event>`
- :ref:`search events by dates <search_event>`
- etc.



Quickstart
=============

- https://caldav.readthedocs.io/en/latest/#quickstart


All code examples below was snippets from the basic_usage_examples.py,
but the documentation and the examples may have drifted apart (TODO: does
there exist some good system for this? Just use docstrings and doctests?)

Setting up a caldav client object and a principal object


.. code-block:: python

    with caldav.DAVClient(url=url, username=username, password=password) as client:
        dav_principal = client.principal()
        ...


.. _creating_a_calendar:

Creating a calendar
===================


.. code-block:: python

    test_calendar = dav_principal.make_calendar(name="First test calendar from Python caldav")


Création de l'agenda "Absences SAGE"
-------------------------------------------

Création de l'agenda "Absences SAGE"
(NEXTCLOUD_JUPYTER_CALENDRIER_ABSENCES = "Absences SAGE")

::

    import os
    import caldav
    import icalendar
    from django.conf import settings


    caldav_url = settings.NEXTCLOUD_JUPYTER_CALDAV_URL
    username =settings.NEXTCLOUD_JUPYTER_USERNAME
    password = os.environ["MY_IMPRONONCIABLE"]
    client = caldav.DAVClient(url=caldav_url, username=username, password=password)
    dav_principal = client.principal()
    dav_principal.make_calendar(name=settings.NEXTCLOUD_JUPYTER_CALENDRIER_ABSENCES)

::

    In [3]:     caldav_url = settings.NEXTCLOUD_JUPYTER_CALDAV_URL
       ...:     username =settings.NEXTCLOUD_JUPYTER_USERNAME
       ...:

    In [4]:     password = os.environ["MY_IMPRONONCIABLE"]
       ...:     client = caldav.DAVClient(url=caldav_url, username=username, password=password)
       ...:     dav_principal = client.principal()
       ...:

    In [5]:     dav_principal.make_calendar(name=settings.NEXTCLOUD_JUPYTER_CALENDRIER_ABSENCES)
       ...:
    Out[5]: Calendar(https://cloud.jupyter.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/af9b36fc-3fee-11ed-a91f-0242ac130002/)


.. _fetching_calendars:

Fetching calendars
====================

.. code-block:: python

   calendars = dav_principal.calendars()

::

    In [35]: calendars
    Out[35]:
    [Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/personal/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/intranet/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/contact_birthdays/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/calendrier-absences/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/test-agenda/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/d1e576ce-3d95-11ed-91df-0242ac140002/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/personal_shared_by_A85F874B-7468-4D99-9744-3DB0D2229F71/),
     Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/personal_shared_by_45F7422B-982F-4B40-A1EB-2E61CA5515C4/)]

    In [36]: c5 = calendars[5]

    In [37]: c5
    Out[37]: Calendar(https://nuage.magellan.eu/remote.php/dav/calendars/BDA3E7B3-F1C5-412B-9A68-2F561CAB268E/d1e576ce-3d95-11ed-91df-0242ac140002/)

    In [38]: c5.name
    Out[38]: 'First test calendar from Python caldav'



.. _find_a_calendar:

Find a calendar with its name
=================================

.. code-block:: python

    import os
    import caldav
    import icalendar
    from django.conf import settings


    caldav_url = settings.NEXTCLOUD_ID3_CALDAV_URL
    username =settings.NEXTCLOUD_ID3_USERNAME
    password = os.environ["MY_IMPRONONCIABLE"]
    client = caldav.DAVClient(url=caldav_url, username=username, password=password)
    dav_principal = client.principal()
    try:
        calendar_name = "sage_absences"
        the_calendar = dav_principal.calendar(name=calendar_name)
        the_calendar.delete()
    except Exception as error:
        logger.error(f"{calendar_name=} {error}")



.. _delete_a_calendar:

Delete a calendar
===================


Example 1
--------------

.. code-block:: python

    import os
    import caldav
    import icalendar
    from django.conf import settings


    caldav_url = settings.NEXTCLOUD_ID3_CALDAV_URL
    username =settings.NEXTCLOUD_ID3_USERNAME
    password = os.environ["MY_IMPRONONCIABLE"]
    client = caldav.DAVClient(url=caldav_url, username=username, password=password)
    dav_principal = client.principal()
    try:
        calendar_name = "sage_absences"
        the_calendar = dav_principal.calendar(name=calendar_name)
        the_calendar.delete()
    except Exception as error:
        logger.error(f"{calendar_name=} {error}")

.. _adding_an_event_parameters:

Create an event to the calendar
=====================================================


Example 1
-----------

.. code-block:: python

    my_event = my_calendar.save_event(
        dtstart=datetime.datetime(2020,5,17,8),
        dtend=datetime.datetime(2020,5,18,1),
        summary="Do the needful",
        rrule={'FREQ': 'YEARLY'))


Example 2
-----------

.. code-block:: python
   :linenos:

    for absence in absences_employe:
        summary = absence.str_agenda()
        description = f"SAGE {summary}"
        location = "Congés/Absences"
        if absence.is_journee_complete():
            dtstart = current_day.set(hour=0, minute=0, second=0)
            dtend = current_day.add(days=1).set(
                hour=0, minute=0, second=0
            )
            # conversion vers date
            dtstart = pendulum_to_date(dtstart)
            dtend = pendulum_to_date(dtend)
        else:
            if absence.is_demi_journee_matin():
                dtstart = current_day.set(hour=8)
                dtend = current_day.set(hour=12)
            if absence.is_demi_journee_apres_midi():
                dtstart = current_day.set(hour=13)
                dtend = current_day.set(hour=17)
            # conversion vers datetime
            dtstart = pendulum_to_datetime(dtstart)
            dtend = pendulum_to_datetime(dtend)
            logger.info(f"Absence demi-journée {dtstart=} => {dtend=}")

        try:
            # Ecriture de l'événement
            new_event = the_calendar.save_event(
                dtstart=dtstart,
                dtend=dtend,
                summary=summary,
                description=description,
                location=location,
            )
            logger.info(
                f"Successfull insertion in calendar {summary} {dtstart=} {dtend=}"
            )
        except Exception as error:
            logger.error(f"{error=}")



.. _delete_an_event:

Delete en event
==================

.. code-block:: python
   :linenos:

    liste_events = the_calendar.date_search(
        start=dtstart_search, end=dtend_search, expand=True
    )
    if len(liste_events) > 0:
        for event in liste_events:
            event.delete()



.. _get_all_events:

Get all events from a calendar
=================================

Get all events from a calendar


.. code-block:: python

   all_events = my_calendar.events()


.. _search_event:

Do a date search in a calendar
==================================

Do a date search in a calendar


Example 1
------------

.. code-block:: python
   :linenos:

    events_fetched = my_calendar.date_search(
        start=datetime(2021, 1, 1), end=datetime(2024, 1, 1), expand=True)


Example 2
------------

- :ref:`pendulum_to_datetime`


.. code-block:: python
   :linenos:

    client = caldav.DAVClient(
        url=settings.NEXTCLOUD_ID3_CALDAV_URL,
        username=settings.NEXTCLOUD_ID3_USERNAME,
        password=password,
    )
    dav_principal = client.principal()
    try:
        calendar_name = settings.NEXTCLOUD_ID3_CALENDRIER_ABSENCES
        logger.info(f"Searching {calendar_name=} ...")
        the_calendar = dav_principal.calendar(name=calendar_name)
    except Exception as error:
        logger.error(f"{calendar_name=} {error}")

    dtstart_search = pendulum.datetime(year, month, 1, tz=settings.TIME_ZONE).start_of(
        "day"
    )
    dtend_search = dtstart_search.end_of("month").end_of("day")
    # conversion vers datetime
    dtstart_search = pendulum_to_datetime(dtstart_search)
    dtend_search = pendulum_to_datetime(dtend_search)
    liste_events = the_calendar.date_search(
        start=dtstart_search, end=dtend_search, expand=True
    )
    if len(liste_events) > 0:
        for event in liste_events:
            event.delete()


.. _create_an_event_ical:

Create an event described through some ical text
=====================================================


Adding an event described through some ical text


.. code-block:: python

    my_event = my_new_calendar.save_event("""BEGIN:VCALENDAR
    VERSION:2.0
    PRODID:-//Example Corp.//CalDAV Client//EN
    BEGIN:VEVENT
    UID:20200516T060000Z-123401@example.com
    DTSTAMP:20200516T060000Z
    DTSTART:20200517T060000Z
    DTEND:20200517T230000Z
    RRULE:FREQ=YEARLY
    SUMMARY:Do the needful
    END:VEVENT
    END:VCALENDAR
    """)

Find a calendar with a known URL without going through the Principal-object
===============================================================================

Find a calendar with a known URL without going through the Principal-object:


.. code-block:: python

   the_same_calendar = client.calendar(url=my_new_calendar.url)


.. index::
   pair: files ; Python

.. _python_files:

==========================
Python files
==========================


.. toctree::
   :maxdepth: 5


   pathlib/pathlib
   pyfilesystem2/pyfilesystem2
   fileutils/fileutils

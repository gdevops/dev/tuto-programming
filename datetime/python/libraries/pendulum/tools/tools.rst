.. index::
   pair: pendulum; tools
   ! pendulum_to_datetime
   ! datetime_to_pendulum

.. _pendulum_tools:

=====================================
**pendulum tools**
=====================================



.. _datetime_to_pendulum:

datetime_to_pendulum
======================


.. code-block:: python
   :linenos:

    def datetime_to_pendulum(date_source: datetime.datetime) -> pendulum.DateTime:
        """Transforme une datetime.datetime en pendulum.DateTime avec une
        éventuelle conversion de fuseau horaire (settings.TIME_ZONE).

        Examples:

            from tools.utils_time import datetime_to_pendulum

            created = datetime_to_pendulum(self.created)
        """
        pendulum_date = pendulum.datetime(
            date_source.year,
            date_source.month,
            date_source.day,
            hour=date_source.hour,
            minute=date_source.minute,
            second=date_source.second,
            tz=date_source.tzinfo,
        ).in_timezone(settings.TIME_ZONE)

        return pendulum_date

.. _date_to_pendulum:

date_to_pendulum
======================


.. code-block:: python
   :linenos:

    def date_to_pendulum(date_source: datetime.date) -> pendulum.DateTime:
        """Transforme une datetime.datetime en pendulum.DateTime avec une
        éventuelle conversion de fuseau horaire (settings.TIME_ZONE).

        Examples:

            from tools.utils_time import date_to_pendulum

            created = date_to_pendulum(self.created)
        """
        pendulum_date = pendulum.datetime(
            date_source.year,
            date_source.month,
            date_source.day,
        ).in_timezone(settings.TIME_ZONE)

        return pendulum_date


.. _pendulum_to_date:

pendulum_to_datetime
======================

.. code-block:: python
   :linenos:

    def pendulum_to_date(date_source: pendulum.DateTime) -> datetime.date:
        """Transforme une pendulum.DateTime en datetime.date."""
        date = datetime.date(
            date_source.year,
            date_source.month,
            date_source.day,
        )

        return date


.. _pendulum_to_datetime:

pendulum_to_datetime
======================


.. code-block:: python
   :linenos:

    def pendulum_to_datetime(date_source: pendulum.DateTime) -> datetime.datetime:
        """Transforme une pendulum.DateTime en datetime.datetime.
        In [1]: import datetime
        In [2]: d = datetime.datetime.now()

        ╭───────────────────────── <class 'datetime.datetime'> ──────────────────────────╮
        │ datetime(year, month, day[, hour[, minute[, second[, microsecond[,tzinfo]]]]]) │
        │                                                                                │
        │ ╭────────────────────────────────────────────────────────────────────────────╮ │
        │ │ datetime.datetime(2021, 11, 25, 6, 15, 19, 595852)                         │ │
        │ ╰────────────────────────────────────────────────────────────────────────────╯ │
        │                                                                                │
        │         day = 25                                                               │
        │        fold = 0                                                                │
        │        hour = 6                                                                │
        │         max = datetime.datetime(9999, 12, 31, 23, 59, 59, 999999)              │
        │ microsecond = 595852                                                           │
        │         min = datetime.datetime(1, 1, 1, 0, 0)                                 │
        │      minute = 15                                                               │
        │       month = 11                                                               │
        │  resolution = datetime.timedelta(microseconds=1)                               │
        │      second = 19                                                               │
        │      tzinfo = None                                                             │
        │        year = 2021                                                             │
        ╰────────────────────────────────────────────────────────────────────────────────╯

        """
        date_datetime = datetime.datetime(
            date_source.year,
            date_source.month,
            date_source.day,
            date_source.hour,
            date_source.minute,
            date_source.second,
        )

        return date_datetime

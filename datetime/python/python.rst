.. index::
   pair: date ; Python Programming

.. _python_date_programming:

=====================================
Python datetime
=====================================

- https://github.com/python/cpython/tree/master/Doc


.. toctree::
   :maxdepth: 6

   datetime/datetime
   zoneinfo/zoneinfo
   libraries/libraries

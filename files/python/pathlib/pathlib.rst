
.. index::
   pair: pathlib ; Python
   ! pathlib

.. _python_pathlib:

==========================
**Python pathlib module**
==========================

.. figure:: images/cheat_sheet.png
   :align: center

   https://github.com/chris1610/pbpython/blob/master/extras/Pathlib-Cheatsheet.pdf

.. toctree::
   :maxdepth: 3


   tutorials/tutorials

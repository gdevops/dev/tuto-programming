.. index::
   pair: date class; Python

.. _python_date_class:

=====================================
Python **date class**
=====================================

.. seealso::

   - https://docs.python.org/dev/library/datetime.html#date-objects




today
======

::

    classmethod date.today()

        Return the current local date.


This is equivalent to date.fromtimestamp(time.time()).

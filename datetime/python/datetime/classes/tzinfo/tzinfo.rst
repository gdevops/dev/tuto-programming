.. index::
   pair: datetime tzinfo; Python

.. _python_tzinfo_class:

=====================================
Python **tzinfo class**
=====================================

.. seealso::

   - https://docs.python.org/dev/library/datetime.html#tzinfo-objects
   - https://docs.python.org/dev/library/datetime.html#datetime.timezone





class datetime.tzinfo
=========================

This is an abstract base class, meaning that this class should not be instantiated
directly.

Define a subclass of tzinfo to capture information about a particular time zone.

An instance of (a concrete subclass of) tzinfo can be passed to the constructors
for datetime and time objects.
The latter objects view their attributes as being in local time, and the
tzinfo object supports methods revealing offset of local time from UTC,
the name of the time zone, and DST offset, all relative to a date or time
object passed to them.

You need to derive a concrete subclass, and (at least) supply implementations
of the standard tzinfo methods needed by the datetime methods you use.

The datetime module provides timezone, a simple concrete subclass of tzinfo
which can represent timezones with fixed offset from UTC such as UTC itself
or North American EST and EDT.

Special requirement for pickling: A tzinfo subclass must have an __init__()
method that can be called with no arguments, otherwise it can be pickled but
possibly not unpickled again.
This is a technical requirement that may be relaxed in the future.

A concrete subclass of tzinfo may need to implement the following methods.
Exactly which methods are needed depends on the uses made of aware datetime
objects. If in doubt, simply implement all of them.

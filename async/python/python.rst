
.. index::
   pair: Python asynchronous ; Programming

.. _python_async:

=================================
Python asynchronous programming
=================================

.. seealso::

   - https://docs.python.org/3/library/asyncio.html
   - https://docs.python.org/3/library/asyncio-api-index.html
   - https://github.com/florimondmanca/awesome-asgi


.. toctree::
   :maxdepth: 3

   examples/examples
   api/api
   libraries/libraries
   tutorials/tutorials

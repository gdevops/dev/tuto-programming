
.. index::
   pair: MariaDB ; timestamp

.. _mariadb_timestamp:

=====================================
MariaDB **timestamp**
=====================================

- https://mariadb.com/kb/en/timestamp/



Time Zones
==============

If a column uses the TIMESTAMP data type, then any inserted values are
converted from the session's time zone to **Coordinated Universal Time (UTC)**
when stored

::

    select utc_timestamp(), current_timestamp(),now();



Setting the default_time_zone (please use UTC timezone)
===========================================================

- :ref:`conf_server_debian_10`


UTC datetime checking
--------------------------

::

    SELECT @@global.time_zone, @@session.time_zone;



With UTC timezone set UTC_TIMESTAMP() is egal to NOW()::


    SELECT UNIX_TIMESTAMP(), UTC_TIMESTAMP(), NOW();

.. figure:: _static/katie_bouman.jpeg
   :width: 150
   :align: center

   https://en.wikipedia.org/wiki/Katie_Bouman


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-programming/rss.xml>`_


.. _programming_tuto:

=======================
**Tuto Programming**
=======================


.. toctree::
   :maxdepth: 4

   people/people

.. toctree::
   :maxdepth: 8

   news/news
   async/async
   calendar/calendar
   cron/cron
   i18n/i18n
   datetime/datetime
   string/string
   files/files
   serialisation/serialisation
   glossary/glossary

.. index::
   pair: python ; icalendar
   ! icalendar

.. _python_icalendar:

==========================================================================
**The Python icalendar package: a parser/generator for iCalendar files**
==========================================================================


- https://github.com/collective/icalendar
- https://icalendar.readthedocs.io/en/latest/usage.html




Wikipedia definition
========================

- https://fr.wikipedia.org/wiki/ICalendar


Description
=============

The Python `icalendar` package is a `RFC 5545 <https://www.ietf.org/rfc/rfc5545.txt>`_ compatible parser/generator
for iCalendar files.

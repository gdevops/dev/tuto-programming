
.. _python_calendar_icalendar:

=====================================
Python calendar/icalendar
=====================================


.. toctree::
   :maxdepth: 6

   calendar/calendar
   pendulum/pendulum
   tools/tools


.. index::
   pair: Black hole; 2019-04-10

.. _black_hole_2019_04_10:

=======================
2019-04-10 black hole
=======================



katie-bouman
==============
.. seealso::

   - https://iopscience.iop.org/article/10.3847/2041-8213/ab0e85
   - http://m.leparisien.fr/societe/qui-est-katie-bouman-l-ingenieure-derriere-la-celebre-photo-du-trou-noir-11-04-2019-8051151.php

.. figure:: profil_facebook.png
   :align: center


« Je suis en train d’observer - sans y croire - la reconstitution en direct de
la toute première image d’un trou noir », détaille la légende de sa photo.

Sur l’écran de son ordinateur, entre plusieurs fenêtres montrant plusieurs
lignes de code, on aperçoit les fragments de cette image partagée des milliers
de fois mercredi : celle d’un anneau orangé sur un fond noir, cliché conçu
après des années de travail combinant astrophysique, mathématiques, ingénierie
et informatique.


.. figure:: hamilton_bouman.jpeg
   :align: center


Free Software and Python
=========================

.. seealso::

   - https://www.tfir.io/gnu-gplv3-at-the-heart-of-black-hole-image/
   - https://github.com/achael/eht-imaging/blob/master/LICENSE.txt


Now, something for the Free and Open Source fans.
The image was created using software defined telescopes that virtually
networked physical telescopes installed in different parts of the world to
create a telescope as big as Earth.

Free and Open Source software was at the heart of this image.

The team used three different imaging software libraries to achieve the
feat. Out of the three, two were fully open source libraries, Sparselab
and ehtim.

Sparselab is a Python Library for Interferometric Imaging using Sparse Modeling.
ehtim is a Python module for simulating and manipulating VLBI data and
producing images with regularized maximum likelihood methods.

Richard M Stallman, the founder of the GNU Project will be glad to see that
the source code for both libraries is released under GNU GPL v3.

Yes, you read it right – GNU GPL v3.

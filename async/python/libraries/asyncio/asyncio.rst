.. index::
   pair: Python asynchronous ; asyncio
   ! asyncio

.. _asyncio:

==========================================================================================
**asyncio** Asynchronous I/O
==========================================================================================

.. seealso::

   - https://docs.python.org/3/library/asyncio.html
   - https://github.com/python/cpython/tree/master/Lib/asyncio
   - :ref:`summary_anyio`
   - https://docs.python.org/3/library/asyncio-task.html
   - https://docs.python.org/3/library/asyncio-stream.html
   - https://docs.python.org/3/library/asyncio-sync.html
   - https://docs.python.org/3/library/asyncio-subprocess.html
   - https://docs.python.org/3/library/asyncio-queue.html
   - https://docs.python.org/3/library/asyncio-exceptions.html
   - https://docs.python.org/3/library/asyncio-api-index.html
   - https://github.com/timofurrer/awesome-asyncio




Description
===========

**asyncio is a library to write concurrent code using the async/await syntax**.

**asyncio** is used as a foundation for multiple Python asynchronous frameworks
that provide high-performance network and web-servers, database connection
libraries, distributed task queues, etc.

**asyncio** is often a perfect fit for IO-bound and high-level structured
network code.

**asyncio** provides a set of **high-level APIs** to:

- run Python coroutines concurrently and have full control over their execution;
- perform network IO and IPC;
- control subprocesses;
- distribute tasks via queues;
- synchronize concurrent code;

Additionally, there are low-level APIs for library and framework developers to:

- create and manage event loops, which provide asynchronous APIs for
  networking, running subprocesses, handling OS signals, etc;
- implement efficient protocols using transports;
- bridge callback-based libraries and code with async/await syntax.


. _asyncio_high_level_api:

**asyncio** High-level APIs
=============================

- `Coroutines and Tasks <https://docs.python.org/3/library/asyncio-task.html>`_
- `Streams <https://docs.python.org/3/library/asyncio-stream.html>`_
- `Synchronization Primitives <https://docs.python.org/3/library/asyncio-sync.html>`_
- `Subprocesses <https://docs.python.org/3/library/asyncio-subprocess.html>`_
- `Queues <https://docs.python.org/3/library/asyncio-queue.html>`_
- `Exceptions <https://docs.python.org/3/library/asyncio-exceptions.html>`_



Guides and Tutorials
========================

- `High-level API Index <https://docs.python.org/3/library/asyncio-api-index.html>`_
- `Developing with asyncio <https://docs.python.org/3/library/asyncio-dev.html>`_

Structured concurrency with **anyio**
=========================================

.. seealso::

   - :ref:`structured_concurrency`
   - :ref:`summary_anyio`
   - :ref:`anyio`
